import 'jest'
import * as React from 'react'
import HighlightedText from '../../src/components/HighlightedText'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:HighlightedText', () => {
  it('renders the content', () => {
    const wrapper = Enzyme.mount(
      <HighlightedText
        text={'Why hello there'}
        highlight={'hello'}
      />
    )

    expect(wrapper.text()).toEqual('Why hello there')
  })

  it('places the highlighted prop text correctly in the tree', () => {
    const wrapper = Enzyme.mount(
      <HighlightedText
        text={'Why hello there'}
        highlight={'hello'}
      />
    )

    expect(wrapper
      .find('HighlightedText')
      .length
    ).toBe(1)

    expect(wrapper
      .find('Styled(span)')
      .filterWhere(n => n.contains('hello'))
      .length
    ).toBe(1)
  })

  it('returns plain unhighlighted text when the match is not found', () => {
    const wrapper = Enzyme.mount(
      <HighlightedText
        text={'Why hello there'}
        highlight={'foo'}
      />
    )

    expect(wrapper
      .find('Styled(span)')
      .filterWhere(n => n.contains('foo'))
      .length
    ).toBe(0)
  })
})