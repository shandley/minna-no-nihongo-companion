import 'jest'
import * as React from 'react'
import Panel from '../../../src/components/Panel'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Panel', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <Panel />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('receives the borderTop prop', () => {
    const wrapper = Enzyme.mount(
      <Panel borderTop={'#CCCCCC'} />
    )

    expect(wrapper
      .find('Styled(div)')
      .length
    ).toBe(1)

    expect(wrapper
      .find('Styled(div)')
      .prop('borderTop')
    ).toEqual('#CCCCCC')
  })
})

