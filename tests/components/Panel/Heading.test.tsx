import 'jest'
import * as React from 'react'
import Panel, { PanelHeading } from '../../../src/components/Panel'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Panel:Heading', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <Panel>
        <PanelHeading
          heading={'title'}
        />
      </Panel>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders the heading', () => {
    const wrapper = Enzyme.mount(
      <Panel>
        <PanelHeading
          heading={'title'}
        />
      </Panel>
    )

    expect(wrapper
      .find('Styled(h2)')
      .length
    ).toBe(1)

    expect(wrapper
      .find('Styled(h2)')
      .text()
    ).toBe('title')
  })

  it('renders the subheading when provided as a string', () => {
    const wrapper = Enzyme.mount(
      <Panel>
        <PanelHeading
          subheading={'subsub'}
        />
      </Panel>
    )

    expect(wrapper
      .find('Styled(h4)')
      .length
    ).toBe(1)

    expect(wrapper
      .find('Styled(h4)')
      .text()
    ).toBe('subsub')
  })

  it('renders the subheading when provided as an array of string', () => {
    const wrapper = Enzyme.mount(
      <Panel>
        <PanelHeading
          subheading={['one', 'two']}
        />
      </Panel>
    )

    expect(wrapper
      .find('Styled(h4)')
      .length
    ).toBe(2)

    expect(wrapper
      .find('Styled(h4)')
      .at(0)
      .text()
    ).toBe('one')

    expect(wrapper
      .find('Styled(h4)')
      .at(1)
      .text()
    ).toBe('two')
  })
})

