import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../../datamocks/store'
import Option from '../../../../src/components/Base/Quiz/Option'
import { mockActivityState as activity } from '../../../datamocks/state'
import { options, minChapterOption } from '../../../datamocks/options'
import { OptionKey, OptionDependencyRule } from '../../../../src/interfaces'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Base:Quiz:Option', () => {
  let store: ReturnType<typeof configureMockStore>

  beforeEach(() => {
    store = configureMockStore({
      activity
    })
  })

  it('renders a number option', () => {
    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Option activityId={4} option={minChapterOption} options={options} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('recognises an option dependency', () => {
    const optionWithDependency = {
      ...minChapterOption,
      dependency: {
        option: OptionKey.MAX_CHAPTER,
        rule: 'min' as OptionDependencyRule
      }
    }

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Option activityId={4} option={optionWithDependency} options={options} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

})
