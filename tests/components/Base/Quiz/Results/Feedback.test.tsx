import 'jest'
import * as React from 'react'
import Feedback from '../../../../../src/components/Base/Quiz/Results/Feedback'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import { missingFeedback, matchFeedback } from '../../../../datamocks/feedback'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Base:Quiz:Results:Feedback', () => {
  it('returns null when no feedback is provided', () => {
    const wrapper = Enzyme.mount(
      <Feedback feedback={null} submittedAnswer={[]} />
    )

    expect(wrapper.isEmptyRender()).toBeTruthy()
  })

  it('presents the feedback to the user for matched results', () => {
    const submittedAnswer = 'いまあめがふってありますか'

    const wrapper = Enzyme.mount(
      <Feedback feedback={matchFeedback} submittedAnswer={[submittedAnswer]} />
    )

    expect(wrapper.find('HighlightedText').text()).toContain(submittedAnswer)
    expect(wrapper.text()).toContain(matchFeedback.advice)
  })

  it('presents the feedback to the user for matched results', () => {
    const wrapper = Enzyme.mount(
      <Feedback feedback={missingFeedback} submittedAnswer={['']} />
    )

    expect(wrapper.text()).toContain(missingFeedback.advice)
  })
})
