import 'jest'
import * as React from 'react'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import Result from '../../../../../src/components/Base/Quiz/Results/Result'
import { MockActivity } from '../../../../datamocks/activities'
import { correctResult, incorrectResult } from '../../../../datamocks/results'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Base:Quiz:Results:Result', () => {
  it('formats correctly for a correct result', () => {
    const wrapper = Enzyme.mount(
      <Result result={correctResult} currentActivity={new MockActivity()} />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('formats correctly for an incorrect result', () => {
    const wrapper = Enzyme.mount(
      <Result result={incorrectResult} currentActivity={new MockActivity()} />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })
})
