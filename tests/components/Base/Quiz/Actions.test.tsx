import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../../datamocks/store'
import Actions from '../../../../src/components/Base/Quiz/Actions'
import { mockActivityState as activity } from '../../../datamocks/state'
import { Wrapper } from '../../../datamocks/interfaces'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Base:Quiz:Actions', () => {
  let wrapper: Wrapper
  let store: ReturnType<typeof configureMockStore>

  beforeEach(() => {
    store = configureMockStore({
      activity
    })

    wrapper = Enzyme.mount(
      <Provider store={store}>
        <Actions />
      </Provider>
    )
  })

  it('renders the two options', () => {
    expect(wrapper
      .find('button')
      .length
    ).toBe(2)
  })

  it('buttons dispatch the correct actions', () => {
    wrapper
      .find('button')
      .at(1)
      .simulate('click')

    wrapper
      .find('button')
      .at(0)
      .simulate('click')

    const actions = store.getActions()

    expect(actions[0].type).toBe('SKIP_QUESTION')
    expect(actions[1].type).toBe('RESET')
  })
})
