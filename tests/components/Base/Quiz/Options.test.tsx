import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../../datamocks/store'
import Options from '../../../../src/components/Base/Quiz/Options'
import { mockActivityState as activity } from '../../../datamocks/state'
import { GrammarTester } from '../../../../src/activities'
import { chapterRangeOptions } from '../../../../src/data/options'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Base:Quiz:Options', () => {
  it('returns null when the activity has no options', () => {
    const wrapper = Enzyme.mount(
      <Options activity={activity} />
    )

    expect(wrapper.isEmptyRender()).toBe(true)
  })

  it('returns min and max chapter options when configured', () => {
    const store = configureMockStore({
      activity
    })

    const activityProp = {
      ...activity,
      currentActivity: new GrammarTester()
    }

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Options activity={activityProp} />
      </Provider>
    )

    expect(wrapper
      .find('Option')
      .at(0)
      .text()
    ).toContain(chapterRangeOptions[0].title)

    expect(wrapper
      .find('Option')
      .at(1)
      .text()
    ).toContain(chapterRangeOptions[1].title)
  })
})
