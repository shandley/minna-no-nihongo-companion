import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import AdjectiveDictionary from '../../../src/components/AdjectiveDictionary'
import { mockListState as list } from '../../datamocks/state'
import adjectives, { Adjective } from 'minna-no-nihongo-adjectives'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:AdjectiveDictionary', () => {
  let store: ReturnType<typeof configureMockStore>

  beforeEach(() => {
    store = configureMockStore({})
  })

  it('renders the component', () => {
    store = configureMockStore({list})

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <AdjectiveDictionary />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('dispatches an action to filter verbs when text is entered into the input', () => {
    store = configureMockStore({list})

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <AdjectiveDictionary />
      </Provider>
    )

    wrapper
      .find('input')
      .simulate('change', { target: { value: 'お' } })

    expect(store.getActions()).toContainEqual({
      type: 'SET_ADJECTIVES',
      payload: adjectives.filter(
        (a: Adjective) => a.hiragana[0] === 'お'
      )
    })
  })
})

