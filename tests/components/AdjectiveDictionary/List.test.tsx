import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import adjectives from 'minna-no-nihongo-adjectives'
import List from '../../../src/components/AdjectiveDictionary/List'
import { mockActivityState as activity } from '../../datamocks/state'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:AdjectiveDictionary:List', () => {
  it('renders the component', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <List adjectives={adjectives.slice(0, 10)} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('dispatches SHOW_MODAL when a row is clicked', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <List adjectives={adjectives.slice(0, 10)} />
      </Provider>
    )

    wrapper.find('tr').at(8).simulate('click')

    const actions = store.getActions()

    expect(actions[0].type).toBe('@redux-modal/SHOW')
  })
})

