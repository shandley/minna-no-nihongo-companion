import 'jest'
import * as React from 'react'
import GrammarRuleItem from '../../src/components/GrammarRuleItem'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import { grammarRules } from '../../src/data/grammar/rules'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:GrammarRuleItem', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <GrammarRuleItem
        rule={grammarRules[3]}
        destroy={() => 42}
      />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders the grammar item details', () => {
    const callback = jest.fn()

    const wrapper = Enzyme.mount(
      <GrammarRuleItem
        rule={grammarRules[3]}
        destroy={callback}
      />
    )

    expect(wrapper
      .find('button')
      .length
    ).toBe(1)

    wrapper.find('button').at(0).simulate('click')

    expect(callback).toHaveBeenCalled()
  })

  it('does not render particle details if the rule does not contain them', () => {
    const callback = jest.fn()

    const mockRule = {
      id: 0.0,
      particles: [],
      rule: '...',
      explanation: '...'
    }

    const wrapper = Enzyme.mount(
      <GrammarRuleItem
        rule={mockRule}
        destroy={callback}
      />
    )

    expect(wrapper.text()).not.toContain('particles')
  })
})

