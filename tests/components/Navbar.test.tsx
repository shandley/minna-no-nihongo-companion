import 'jest'
import * as React from 'react'
import Navbar from '../../src/components/Navbar'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Navbar', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <Navbar />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })
})

