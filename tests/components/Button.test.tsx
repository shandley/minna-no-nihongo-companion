import 'jest'
import * as React from 'react'
import Button from '../../src/components/Button'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Button', () => {
  it('renders the component', () => {
    const callback = jest.fn()

    const wrapper = Enzyme.mount(
      <Button label="Hello" action={callback} />
    )

    expect(wrapper.find('button').length).toBe(1)
    expect(wrapper.find('button').text()).toBe('Hello')

    wrapper.find('button').at(0).simulate('click')

    expect(callback).toHaveBeenCalled()
  })

  it('disables the button once an answer is submitted', () => {
    const callback = jest.fn()

    const wrapper = Enzyme.mount(
      <Button label="Hello" action={callback} correct={false} />
    )

    expect(wrapper.find('button').length).toBe(1)
    expect(wrapper.find('button').prop('disabled')).toBe(true)
  })
})

