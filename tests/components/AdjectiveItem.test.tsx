import 'jest'
import * as React from 'react'
import AdjectiveItem from '../../src/components/AdjectiveItem'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import adjectives from 'minna-no-nihongo-adjectives'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:AdjectiveItem', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <AdjectiveItem
        adjective={adjectives[3]}
        destroy={() => 42}
      />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders the adjective details', () => {
    const callback = jest.fn(() => 42)
    const wrapper = Enzyme.mount(
      <AdjectiveItem
        adjective={adjectives[0]}
        destroy={callback}
      />
    )

    expect(wrapper
      .find('h4')
      .length
    ).toBe(1)
    
    expect(wrapper
      .find('h4')
      .text()
    ).toContain('-adjective')

    wrapper.find('Button').at(0).simulate('click')
    expect(callback).toHaveBeenCalled()
  })
})

