import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import App from '../../src/components/App'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../datamocks/store'
import { activities } from '../../src/data'
import { mockActivityState, mockListState } from '../datamocks/state'
import { NoActivity, VerbDictionary } from '../../src/activities'

Enzyme.configure({
  adapter: new Adapter(),
})

// This is due to redux-modal having issues which are logged by react
console.warn = jest.fn()

describe('Component:App', () => {
  it('renders the activities list when `NoActivity` is the current activity', () => {
    const store = configureMockStore({
      modal: {},
      activity: {
        ...mockActivityState,
        currentActivity: new NoActivity()
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <App />
      </Provider>
    )

    expect(wrapper
      .find('ActivityPanel')
      .length
    ).toBe(activities.length)
  })

  it('dispatches setActivity when an activity panel button is pressed', () => {
    const store = configureMockStore({
      modal: {},
      activity: {
        ...mockActivityState,
        currentActivity: new NoActivity()
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <App />
      </Provider>
    )

    wrapper
      .find('ActivityPanel')
      .at(0)
      .find('button')
      .simulate('click')

    const actions = store.getActions()

    expect(actions[0].type).toBe('SET_ACTIVITY')
  })

  it('renders an activity when it has been set', () => {
    const store = configureMockStore({
      modal: {},
      list: mockListState,
      activity: {
        ...mockActivityState,
        currentActivity: new VerbDictionary()
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <App />
      </Provider>
    )

    expect(wrapper
      .find('VerbDictionary')
      .length
    ).toBe(1)
  })
})

