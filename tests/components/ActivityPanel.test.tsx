import 'jest'
import * as React from 'react'
import ActivityPanel from '../../src/components/ActivityPanel'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:ActivityPanel', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <ActivityPanel heading={'hello'} subheading={'there'} actions={[]} />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders the component with actions when provided', () => {
    const wrapper = Enzyme.mount(
      <ActivityPanel
        heading={'hello'}
        subheading={'there'}
        actions={[{
          label: 'test',
          action: () => {
            return 42
          }
        }]}
      />
    )

    const button = wrapper.find('Button')

    expect(button
      .length
    ).toBe(1)

    expect(button
      .text()
    ).toBe('test')
  })
})

