import 'jest'
import * as React from 'react'
import VerbItem from '../../src/components/VerbItem'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import verbs from 'minna-no-nihongo-verbs'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:VerbItem', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <VerbItem
        verb={verbs[3]}
        destroy={() => 42}
      />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders the verb details', () => {
    const callback = jest.fn(() => 42)
    const wrapper = Enzyme.mount(
      <VerbItem
        verb={verbs[0]}
        destroy={callback}
      />
    )

    expect(wrapper
      .find('h4')
      .length
    ).toBe(1)

    wrapper.find('Button').at(0).simulate('click')
    expect(callback).toHaveBeenCalled()
  })
})

