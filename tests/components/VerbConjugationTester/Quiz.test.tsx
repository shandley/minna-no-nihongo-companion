import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import Quiz from '../../../src/components/VerbConjugationTester/Quiz'
import { verbConjugationTesterActivity } from '../../datamocks/state'
import { verbConjugationTesterQuestion } from '../../datamocks/questions'
import { theme } from '../../../src/data'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:VerbConjugationTester:Quiz', () => {
  it('renders the component', () => {
    const activity = {
      ...verbConjugationTesterActivity,
      currentQuestion: {
        ...verbConjugationTesterQuestion,
        instructions: null
      },
      running: false,
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('displays instructions for the question when provided', () => {
    const activity = {
      ...verbConjugationTesterActivity,
      running: true,
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper
      .findWhere(n => n.text() === 'Convert from ます Form to て Form')
      .length
    ).toBe(1)
  })

  it('displays the top border as green when the question is correct', () => {
    const activity = {
      ...verbConjugationTesterActivity,
      running: true,
      correct: true
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper
      .find('Quiz')
      .at(0)
      .childAt(0)
      .prop('borderTop')
    ).toBe(theme.colours.green.dark)
  })

  it('displays the top border as red when the question is incorrect', () => {
    const activity = {
      ...verbConjugationTesterActivity,
      running: true,
      correct: false
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper
      .find('Quiz')
      .at(0)
      .childAt(0)
      .prop('borderTop')
    ).toBe(theme.colours.red.dark)
  })

  it('dispatches SUBMIT_ANSWER when an option is clicked', () => {
    const activity = {
      ...verbConjugationTesterActivity,
      running: true,
      correct: false
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    const button = wrapper.find('button').at(0)
    button.simulate('click')

    expect(store.getActions()).toContainEqual({
      type: 'SUBMIT_ANSWER',
      payload: [button.text()]
    })
  })
})
