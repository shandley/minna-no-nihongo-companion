import 'jest'
import * as React from 'react'
import Bar from '../../../src/components/Icon/Bar'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Icon:Bar', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <Bar />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })
})

