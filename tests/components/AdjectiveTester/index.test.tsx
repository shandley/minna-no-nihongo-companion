import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import AdjectiveTester from '../../../src/components/AdjectiveTester'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import { mockActivityState as activity } from '../../datamocks/state'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:AdjectiveTester', () => {
  type Wrapper = Enzyme.ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>

  const has = (
    wrapper: Wrapper,
    component: string
  ) => wrapper.find(component).length === 1

  const hasIntro = (wrapper: Wrapper) => has(wrapper, 'Intro')
  const hasQuiz = (wrapper: Wrapper) => has(wrapper, 'Quiz')
  const hasResults = (wrapper: Wrapper) => has(wrapper, 'Results')

  it('renders the intro when the activity is neither running, nor complete', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <AdjectiveTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(true)
    expect(hasQuiz(wrapper)).toBe(false)
    expect(hasResults(wrapper)).toBe(false)
  })

  it('renders the results when the activity is not running, but the status is complete', () => {
    const store = configureMockStore({
      activity: {
        ...activity,
        complete: true
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <AdjectiveTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(false)
    expect(hasQuiz(wrapper)).toBe(false)
    expect(hasResults(wrapper)).toBe(true)
  })

  it('renders the quiz when the activity is running', () => {
    const store = configureMockStore({
      activity: {
        ...activity,
        running: true
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <AdjectiveTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(false)
    expect(hasQuiz(wrapper)).toBe(true)
    expect(hasResults(wrapper)).toBe(false)
  })
})

