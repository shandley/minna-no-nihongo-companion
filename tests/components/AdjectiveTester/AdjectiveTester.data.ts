import { AdjectiveTester } from "../../../src/activities/adjectiveTester"
import adjectives from 'minna-no-nihongo-adjectives'

const adjectiveItem = adjectives[6]

export const mockActivityState = {
  currentActivity: new AdjectiveTester(),
  currentQuestion: {
    id: '3',
    question: adjectiveItem.hiragana,
    options: [''],
    answer: adjectiveItem.hiragana,
    meta: {
      kanji: adjectiveItem.kanji,
      english: adjectiveItem.english
    }
  },
  options: [],
  questions: [
    {
      id: '3',
      question: adjectiveItem.hiragana,
      options: [''],
      answer: adjectiveItem.hiragana,
      meta: {
        kanji: adjectiveItem.kanji,
        english: adjectiveItem.english
      }
    }
  ][Symbol.iterator](),
  running: false,
  submittedAnswer: [],
  correct: null,
  complete: false,
  results: []
}