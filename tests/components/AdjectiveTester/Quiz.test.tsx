import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import Quiz from '../../../src/components/AdjectiveTester/Quiz'
import { AdjectiveTester } from '../../../src/activities'
import configureMockStore from '../../datamocks/store'
import { mockActivityState as activity } from '../../datamocks/state'
import { theme } from '../../../src/data'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:AdjectiveTester:Quiz', () => {
  const mockActivityRunningState = {
    currentActivity: new AdjectiveTester(),
    currentQuestion: {
      id: '3',
      question: '',
      options: [''],
      answer: [''],
      meta: {
        kanji: 'か'
      }
    },
    options: [],
    questions: [][Symbol.iterator](),
    running: false,
    submittedAnswer: [],
    correct: true,
    complete: false,
    results: []
  }

  it('renders the component', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders the top border as green when the question state is correct', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={{
          ...mockActivityRunningState,
          correct: true
        }} />
      </Provider>
    )

    expect(wrapper
      .find('Styled(div)')
      .first()
      .prop('borderTop')
    ).toEqual(theme.colours.green.dark)
  })

  it('renders the top border as red when the question state is incorrect', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={{
          ...mockActivityRunningState,
          correct: false
        }} />
      </Provider>
    )

    expect(wrapper
      .find('Styled(div)')
      .first()
      .prop('borderTop')
    ).toEqual(theme.colours.red.dark)
  })
})

