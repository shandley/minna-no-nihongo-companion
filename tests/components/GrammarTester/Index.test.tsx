import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import GrammarTester from '../../../src/components/GrammarTester'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import { mockActivityState as activity } from '../../datamocks/state'
import { Wrapper } from '../../datamocks/interfaces'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:GrammarTester', () => {
  const has = (
    wrapper: Wrapper,
    component: string
  ) => wrapper.find(component).length === 1

  const hasIntro = (wrapper: Wrapper) => has(wrapper, 'Intro')
  const hasQuiz = (wrapper: Wrapper) => has(wrapper, 'Quiz')
  const hasResults = (wrapper: Wrapper) => has(wrapper, 'Results')

  it('renders the intro when the activity is neither running, nor complete', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <GrammarTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(true)
    expect(hasQuiz(wrapper)).toBe(false)
    expect(hasResults(wrapper)).toBe(false)
  })

  it('renders the results when the activity is not running, but the status is complete', () => {
    const store = configureMockStore({
      activity: {
        ...activity,
        complete: true
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <GrammarTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(false)
    expect(hasQuiz(wrapper)).toBe(false)
    expect(hasResults(wrapper)).toBe(true)
  })
})

