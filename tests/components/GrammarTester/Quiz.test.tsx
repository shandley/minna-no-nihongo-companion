import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import Quiz from '../../../src/components/GrammarTester/Quiz'
import { grammarTesterActivity, mockActivityState as activity } from '../../datamocks/state'
import { theme } from '../../../src/data'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:GrammarTester:Quiz', () => {
  it('renders the component with a conversation item', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={grammarTesterActivity} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('renders a button for each option', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={grammarTesterActivity} />
      </Provider>
    )

    expect(wrapper
      .find('Button')
      .at(0)
      .prop('label')
    ).toEqual('foo')

    expect(wrapper
      .find('Button')
      .at(1)
      .prop('label')
    ).toEqual('bar')
  })

  it('submits the answer when an option is clicked', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={grammarTesterActivity} />
      </Provider>
    )

    wrapper.find('Button').at(0).simulate('click')

    expect(store.getActions()).toContainEqual({
      type: 'SUBMIT_ANSWER',
      payload: ['foo']
    })
  })

  it('renders the top border as green when the question state is correct', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={{
          ...grammarTesterActivity,
          correct: true
        }} />
      </Provider>
    )

    expect(wrapper
      .find('Styled(div)')
      .first()
      .prop('borderTop')
    ).toEqual(theme.colours.green.dark)
  })

  it('renders the top border as red when the question state is incorrect', () => {
    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store}>
        <Quiz activity={{
          ...grammarTesterActivity,
          correct: false
        }} />
      </Provider>
    )

    expect(wrapper
      .find('Styled(div)')
      .first()
      .prop('borderTop')
    ).toEqual(theme.colours.red.dark)
  })
})
