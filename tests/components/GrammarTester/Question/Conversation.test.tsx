import 'jest'
import * as React from 'react'
import Conversation from '../../../../src/components/GrammarTester/Question/Conversation'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import { IGrammarItem } from '../../../../src/interfaces'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:GrammarTester:Question:Conversation', () => {
  it('renders the component', () => {
    const ruleItemFixture: IGrammarItem = {
      id: 'abc',
      ruleId: 0,
      hiragana: [
        {
          line: 'test'
        },
        {
          line: 'question'
        }
      ]
    }

    const wrapper = Enzyme.mount(
      <Conversation ruleItem={ruleItemFixture} />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })
})

