import 'jest'
import * as React from 'react'
import Activity from '../../src/components/Activity'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import { NoActivity } from '../../src/activities'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:Activity', () => {
  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <Activity activity={new NoActivity()} />
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })
})

