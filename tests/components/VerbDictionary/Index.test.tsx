import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import VerbDictionary from '../../../src/components/VerbDictionary'
import { mockListState as list } from '../../datamocks/state'
import verbs, { Verb } from 'minna-no-nihongo-verbs'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:VerbDictionary', () => {
  let store: ReturnType<typeof configureMockStore>

  beforeEach(() => {
    store = configureMockStore({
      list
    })
  })

  it('renders the component', () => {
    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <VerbDictionary />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('dispatches an action to filter verbs when text is entered into the input', () => {
    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <VerbDictionary />
      </Provider>
    )

    wrapper
      .find('input')
      .simulate('change', { target: { value: 'お' } })

    expect(store.getActions()).toContainEqual({
      type: 'SET_VERBS',
      payload: verbs.filter(
        (v: Verb) => v.polite_form.present[0] === 'お'
      )
    })
  })
})

