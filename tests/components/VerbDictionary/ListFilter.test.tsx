import 'jest'
import * as React from 'react'
import ListFilter from '../../../src/components/VerbDictionary/ListFilter'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:VerbDictionary:ListFilter', () => {
  it('renders the component', () => {
    const callback = jest.fn()

    const wrapper = Enzyme.mount(
      <ListFilter onChange={callback} />
    )

    expect(wrapper.isEmptyRender()).toBe(false)

    expect(wrapper
      .find('input')
      .length
    ).toBe(1)

    wrapper.simulate('change', { target: { value: 'H' } })

    expect(callback).toHaveBeenCalled()
  })
})

