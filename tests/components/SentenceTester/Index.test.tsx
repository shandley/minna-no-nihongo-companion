import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import SentenceTester from '../../../src/components/SentenceTester'
import { SentenceTester as Activity } from '../../../src/activities/sentenceTester'
import { mockActivityState } from '../../datamocks/state'

Enzyme.configure({
  adapter: new Adapter(),
})

type Wrapper = Enzyme.ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>

  const has = (
    wrapper: Wrapper,
    component: string
  ) => wrapper.find(component).length === 1

  const hasIntro = (wrapper: Wrapper) => has(wrapper, 'Intro')
  const hasOptions = (wrapper: Wrapper) => has(wrapper, 'Options')
  const hasQuiz = (wrapper: Wrapper) => has(wrapper, 'Quiz')
  const hasResults = (wrapper: Wrapper) => has(wrapper, 'Results')

describe('Component:SentenceTester', () => {
  it('renders the intro when the activity is not running', () => {
    const store = configureMockStore({
      activity: {
        ...mockActivityState,
        currentActivity: new Activity(),
        running: false
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <SentenceTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(true)
    expect(hasOptions(wrapper)).toBe(true)
    expect(hasQuiz(wrapper)).toBe(false)
    expect(hasResults(wrapper)).toBe(false)
  })

  it('renders the results when the activity is not running, but the status is complete', () => {
    const store = configureMockStore({
      activity: {
        ...mockActivityState,
        currentActivity: new Activity(),
        complete: true
      }
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <SentenceTester />
      </Provider>
    )

    expect(hasIntro(wrapper)).toBe(false)
    expect(hasOptions(wrapper)).toBe(false)
    expect(hasQuiz(wrapper)).toBe(false)
    expect(hasResults(wrapper)).toBe(true)
  })
})
