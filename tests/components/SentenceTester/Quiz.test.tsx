import 'jest'
import * as React from 'react'
import { Provider } from 'react-redux'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import configureMockStore from '../../datamocks/store'
import Quiz from '../../../src/components/SentenceTester/Quiz'
import { sentenceTesterActivity } from '../../datamocks/state'
import { sentenceTesterQuestion } from '../../datamocks/questions'
import { IQuestion } from '../../../src/interfaces'
import { theme } from '../../../src/data'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Component:SentenceTester:Quiz', () => {
  it('renders the component', () => {
    const activity = {
      ...sentenceTesterActivity,
      running: false,
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper.isEmptyRender()).toBe(false)
  })

  it('dispatches an error if no questions are found', () => {
    const activity = {
      ...sentenceTesterActivity,
      currentQuestion: (null as unknown) as IQuestion,
      running: true,
    }

    const store = configureMockStore({
      activity
    })

    Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(store.getActions()).toContainEqual({
      type: 'SET_ERROR',
      payload: 'No questions found'
    })
  })

  it('displays instructions for the question when provided', () => {
    const activity = {
      ...sentenceTesterActivity,
      currentQuestion: sentenceTesterQuestion,
      running: true,
    }

    activity.currentQuestion.meta.instructions = 'HELLO'

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper
      .findWhere(n => n.text() === 'HELLO')
      .length
    ).toBe(1)
  })

  it('displays the top border as green when the question is correct', () => {
    const activity = {
      ...sentenceTesterActivity,
      running: true,
      correct: true
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper
      .find('Quiz')
      .at(0)
      .childAt(0)
      .prop('borderTop')
    ).toBe(theme.colours.green.dark)
  })

  it('displays the top border as red when the question is incorrect', () => {
    const activity = {
      ...sentenceTesterActivity,
      running: true,
      correct: false
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    expect(wrapper
      .find('Quiz')
      .at(0)
      .childAt(0)
      .prop('borderTop')
    ).toBe(theme.colours.red.dark)
  })

  it('does not submit the answer when the enter key is pressed in the form, but the value fails validation', () => {
    const activity = {
      ...sentenceTesterActivity,
      running: true,
    }

    const store = configureMockStore({
      activity
    })

    const wrapper = Enzyme.mount(
      <Provider store={store} >
        <Quiz activity={activity} />
      </Provider>
    )

    wrapper.find('input').simulate('keypress', { key: 'Enter' })

    expect(store.getActions()).toEqual([])
  })
})
