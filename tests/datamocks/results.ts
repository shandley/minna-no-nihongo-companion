export const correctResult = {
  question: {
    id: "MFt9LWgBM",
    answer: [
      "でんしゃ が こなかったら, どう します か？"
    ],
    question: "If the train doesn't come, what should we do?",
    options: [
      ""
    ],
    meta: {
      rule: {
        id: 25.1,
        particles: [
          "ら"
        ],
        rule: "[V.TaForm] [condition] ... [action]",
        explanation: "When 'た' is added after a past tense た verb, it indicates a conditional statement in the form\n      '[if] condition [then] action'. This structure can be used to express a want or hope, which is dependent on \n      something being true before it can be acieved."
      },
      sentence: {
        id: "MFt9LWgBM",
        ruleId: 25.1,
        hiragana: "でんしゃ が こなかったら, どう します か？",
        english: "If the train doesn't come, what should we do?",
        feedback: [
          {
            match: "でんしゃを",
            advice: "Use `が` in this case"
          },
          {
            missing: "こなかったら",
            advice: "The verb for `くる` (come)) becomes `こなかったら` in a negative た-form"
          }
        ]
      },
      instructions: ""
    },
    questionNumber: 1
  },
  submittedAnswer: [
    "でしゃこないったら、どうしますか？"
  ],
  correct: false,
  feedback: {
    missing: "こなかったら",
    advice: "The verb for `くる` (come)) becomes `こなかったら` in a negative た-form"
  }
}

export const incorrectResult = {
  question: {
    id: "0yGVqPEEQ",
    answer: [
      "あした あめ が ふったら, うち で べんきょう します"
    ],
    question: "If it rains tomorrow, I will study at home",
    options: [
      ""
    ],
    meta: {
      rule: {
        id: 25.1,
        particles: [
          "ら"
        ],
        rule: "[V.TaForm] [condition] ... [action]",
        explanation: "When 'た' is added after a past tense た verb, it indicates a conditional statement in the form\n      '[if] condition [then] action'. This structure can be used to express a want or hope, which is dependent on \n      something being true before it can be acieved."
      },
      sentence: {
        id: "0yGVqPEEQ",
        ruleId: 25.1,
        hiragana: "あした あめ が ふったら, うち で べんきょう します",
        english: "If it rains tomorrow, I will study at home"
      },
      instructions: ""
    },
    questionNumber: 2
  },
  submittedAnswer: [
    "あしたあめがふったら、うちでべんきょうします"
  ],
  correct: true,
  feedback: null
}

export const resultsCollection = {
  question_id: "pjchaoBjS",
  results: [
    {
      time: 1569200486241,
      correct: false,
      submittedAnswer: [
        "えきまで"
      ]
    },
    {
      time: 1569200734017,
      correct: false,
      submittedAnswer: [
        "えきにつったら、でんわをしますください"
      ]
    },
    {
      time: 1569201479831,
      correct: false,
      submittedAnswer: [
        "えきにつかったら、でんわをください"
      ]
    },
    {
      time: 1569203897444,
      correct: false,
      submittedAnswer: [
        "えきにつかったら、でんわをください"
      ]
    },
    {
      time: 1569205355758,
      correct: false,
      submittedAnswer: [
        "駅に使ったら、でんわをください"
      ]
    },
    {
      time: 1569205580764,
      correct: false,
      submittedAnswer: [
        "えきにつかったら、でんわをください"
      ]
    },
    {
      time: 1569207036721,
      correct: false,
      submittedAnswer: [
        "えきについったら、でんわをください"
      ]
    },
    {
      time: 1569207320055,
      correct: false,
      submittedAnswer: [
        "えきについったら、でんわをください"
      ]
    }
  ]
}