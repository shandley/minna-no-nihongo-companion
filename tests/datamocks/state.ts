import { IListState } from '../../src/store/reducers/list'
import { NoActivity } from '../../src/activities/noActivity'
import adjectives from 'minna-no-nihongo-adjectives'
import verbs from 'minna-no-nihongo-verbs'
import { MockActivity } from './activities'
import {
  grammarTesterQuestion,
  sentenceTesterQuestion,
  verbConjugationTesterQuestion
} from './questions'

const adjectiveItem = adjectives[7]

export const mockActivityState = {
  currentActivity: new NoActivity(),
  currentQuestion: {
    id: '3',
    question: adjectiveItem.hiragana,
    options: [''],
    answer: [adjectiveItem.hiragana],
    meta: {
      kanji: adjectiveItem.kanji
    }
  },
  options: [],
  questions: [
    {
      id: '3',
      question: adjectiveItem.hiragana,
      options: ['ひま'],
      answer: [adjectiveItem.hiragana],
      meta: {
        kanji: adjectiveItem.kanji,
        english: adjectiveItem.english
      }
    }
  ][Symbol.iterator](),
  running: false,
  submittedAnswer: [],
  correct: null,
  complete: false,
  results: []
}

export const mockListState: IListState = {
  verbs,
  adjectives,
  filteredVerbs: verbs,
  filteredAdjectives: adjectives
}

export const grammarTesterActivity = {
  ...mockActivityState,
  currentActivity: new MockActivity(),
  currentQuestion: grammarTesterQuestion
}

export const sentenceTesterActivity = {
  ...mockActivityState,
  currentActivity: new MockActivity(),
  currentQuestion: sentenceTesterQuestion
}

export const verbConjugationTesterActivity = {
  ...mockActivityState,
  currentActivity: new MockActivity(),
  currentQuestion: verbConjugationTesterQuestion
}