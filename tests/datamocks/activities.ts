import { IQuizActivity, IQuestion, IQuestionResultFeedback } from '../../src/interfaces'

class MockActivity implements IQuizActivity {
  public title = 'Mock Quiz Activity'
  public subtitle = ''
  public type = 99
  public options = []

  public getQuestions = (): Iterator<any> => [][Symbol.iterator]()
  public setGetQuestion = (method: () => Iterator<IQuestion | never>) => this.getQuestions = method

  public evaluateAnswer = () => ({ correct: true })
  public setEvaluateAnswer = (method: () => { correct: boolean }) => this.evaluateAnswer = method

  public renderQuestion = (question: IQuestion) => question.question

  public getFeedback: (
    question: IQuestion,
    submittedAnswer: string[]
  ) => IQuestionResultFeedback | null

  public setGetFeedback = (
    method: () => IQuestionResultFeedback
  ) => this.getFeedback = method
}

export { MockActivity }