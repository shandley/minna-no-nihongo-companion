import Enzyme from 'enzyme'

export type Wrapper = Enzyme.ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>