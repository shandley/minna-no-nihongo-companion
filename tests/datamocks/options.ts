export const options = {
  "1": {
    "NUM_QUESTIONS": 10
  },
  "2": {
    "NUM_QUESTIONS": 10
  },
  "3": {
    "NUM_QUESTIONS": 10,
    "ANSWER_VERB_FORM": "te_form"
  },
  "4": {
    "NUM_QUESTIONS": 10,
    "MAX_CHAPTER": 26,
    "MIN_CHAPTER": 25
  },
  "5": {
    "NUM_QUESTIONS": 10
  },
  "6": {
    "NUM_QUESTIONS": 10
  }
}

export const minChapterOption = {
  id: "MIN_CHAPTER",
  title: "Minimum Chapter",
  type: 1,
  default: 1,
}