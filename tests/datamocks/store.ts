import configureMockStore from '@jedmao/redux-mock-store'
import thunk, { ThunkDispatch } from 'redux-thunk'
import { Action } from 'redux'

// internal dependencies
import { IApplicationState } from '../../src/store/index'
import { ActivityAction, ListAction, ErrorAction } from '../../src/interfaces'

type Actions = Action<ActivityAction> | Action<ListAction> | Action<ErrorAction>

const middlewares = [thunk]

export default configureMockStore<
  IApplicationState,
  Actions,
  ThunkDispatch<IApplicationState, null, Actions>
>(middlewares)