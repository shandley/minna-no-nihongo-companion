import {
  IGrammarTesterQuestion,
  ISentenceTesterQuestion,
  IVerbConjugationTesterQuestion
} from "../../src/interfaces"

export const grammarTesterQuestion = {
  id: '7',
  options: ['foo', 'bar'],
  question: {
    id: 'abc',
    ruleId: 0,
    hiragana: [
      {
        line: 'test'
      },
      {
        line: 'question'
      }
    ]
  },
  answer: ['ひま'],
  meta: {
    ruleId: 23
  }
} as IGrammarTesterQuestion

export const sentenceTesterQuestion = {
  id: "SW8Iu1NlO",
  answer: [
    "としょうかん に いったら、この ほん を かえって ください"
  ],
  question: "If you go to the library, please return this book",
  options: [
    ""
  ],
  meta: {
    rule: {
      id: 25.1,
      particles: [
        "ら"
      ],
      rule: "[V.TaForm] [condition] ... [action]",
      explanation: "When 'た' is added after a past tense た verb, it indicates a conditional statement in the form\n      '[if] condition [then] action'. This structure can be used to express a want or hope, which is dependent on \n      something being true before it can be acieved."
    },
    sentence: {
      id: "SW8Iu1NlO",
      ruleId: 25.1,
      hiragana: "としょうかん に いったら、この ほん を かえって ください",
      english: "If you go to the library, please return this book"
    },
  },
  questionNumber: 1
} as ISentenceTesterQuestion

export const verbConjugationTesterQuestion = {
  id: "B6hHSJiWAaX",
  question: "およぎます",
  answer: [
    "およいで"
  ],
  options: [
    "およっで",
    "およって",
    "およんで",
    "およいで"
  ],
  meta: {
    english: "swim",
    kanji: "泳ぎます",
    instructions: "Convert from ます Form to て Form"
  },
  questionNumber: 1
} as IVerbConjugationTesterQuestion