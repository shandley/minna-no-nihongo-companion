export const matchFeedback = {
  match: "あります",
  advice: "An event that is in progress should be denoted by います"
}

export const missingFeedback = {
  missing: "あつい",
  advice: "Missing adjective: あつい"
}
