import 'jest'
import {
  equalArrays,
  padArrayWith,
  stripWhitespace,
  numberedArrayItems,
  mergeObjects
} from '../src/utils'

describe('utils', () => {
  describe('equalArrays', () => {
    it('should match on [] and []', () => {
      expect(equalArrays([], [])).toEqual(true)
    })

    it('should not match on different arrays', () => {
      expect(equalArrays(['hello', 6], ['good'])).toEqual(false)
    })

    it('should not match on arrays with different order of items', () => {
      expect(equalArrays(['hello', 'goodby'], ['goodbye', 'hello'])).toEqual(false)
    })
  })

  describe('padArrayWith', () => {
    it('should trim an array if the desired length is less than the argument length', () => {
      expect(padArrayWith([1, 2, 3, 4, 5, 6, 7], [9, 9, 9, 9, 9], 3)).toHaveLength(3)
    })
  })

  describe('stripWhitespace', () => {
    it('removes all whitespace', () => {
      expect(stripWhitespace('this is a sentence')).toEqual('thisisasentence')
    })
  })

  describe('numberedArrayItems', () => {
    it('appends question numbers', () => {
      const array = [
        {
          id: 1,
          key: 'value'
        },
        {
          id: 2,
          key: 'another value'
        },
        {
          id: 3,
          kay: 123
        }
      ]

      expect(numberedArrayItems(array)).toEqual([
        {
          id: 1,
          key: 'value',
          questionNumber: 1
        },
        {
          id: 2,
          key: 'another value',
          questionNumber: 2
        },
        {
          id: 3,
          kay: 123,
          questionNumber: 3
        }
      ])
    })

    describe('mergoObjects', () => {
      it('merges simple objects with correct left-side precedence', () => {
        const left = {
          id: 43,
          something: 'hello',
        }

        const right = {
          something: 'hello again'
        }

        expect(mergeObjects(left, right)).toEqual({
          id: 43,
          something: 'hello again'
        })

        expect(mergeObjects(right, left)).toEqual({
          id: 43,
          something: 'hello'
        })
      })
    })
  })  
})
