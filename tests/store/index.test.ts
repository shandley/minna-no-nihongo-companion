import "jest"
import { configureStore } from '../../src/store/index'

describe('store', () => {
  it('configureStore', () => {
    expect(configureStore()).toBeDefined()
  })
})