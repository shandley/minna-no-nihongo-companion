import 'jest'
import * as sinon from 'sinon'
import * as storage from '../../../src/lib/storage'
import { persistResultsMiddleware } from '../../../src/store/middleware/persistResults'
import { ActivityAction } from '../../../src/interfaces'
import { create } from './utils'

const createMiddleware = (state: any) => create(persistResultsMiddleware, state)

describe('middleware:persistResults', () => {
  it('dispatches the next action when the action is not RECORD_RESULT', () => {
    const { next, invoke } = createMiddleware({})

    const action = { type: ActivityAction.START }
    invoke(action)

    expect(next).toHaveBeenCalledWith(action)
  })

  it('makes a call to persist the option then the action is RECORD_RESULT', () => {
    const activityState = {
      activity: {
        currentQuestion: {},
        submittedAnswer: '',
        correct: false
      }
    }

    sinon.mock(storage).expects('persistResult').calledOnceWithExactly(activityState)

    const { next, invoke } = createMiddleware(activityState)

    const action = { type: ActivityAction.RECORD_RESULT }
    invoke(action)

    expect(next).toHaveBeenCalledWith(action)
  })
})