import "jest"
import configureMockStore from '../../datamocks/store'
import * as actions from '../../../src/store/actions/activity'
import { ActivityAction } from '../../../src/interfaces/activity'
import { MockActivity } from "../../datamocks/activities"
import { NoActivity } from "../../../src/activities/noActivity"

describe('actions:activity', () => {
  let store: ReturnType<typeof configureMockStore>

  beforeEach(() => {
    store = configureMockStore({})
  })

  it('startActivity', () => {
    expect(actions.startActivity()).toEqual({
      type: ActivityAction.START
    })
  })

  it('setOption', () => {
    expect(actions.setOption('SOME_OPTION')).toEqual({
      type: ActivityAction.SET_OPTION,
      payload: 'SOME_OPTION'
    })
  })

  it('setActivity', () => {
    const payload = new MockActivity()

    store.dispatch(actions.setActivity(payload))

    const expectedDispatch = {
      type: 'SET_ACTIVITY',
      payload
    }

    expect(store.getActions()).toContainEqual(expectedDispatch)
  })

  it('backToMenu', () => {
    const payload = new NoActivity()

    store.dispatch(actions.backToMenu())

    const expectedDispatch = {
      type: 'SET_ACTIVITY',
      payload
    }

    // The object contains functions, which serialize
    // a little differently in normal comparisons
    expect(
      JSON.stringify(store.getActions())
    ).toEqual(
      JSON.stringify([expectedDispatch])
    )
  })

  it('quitActivity', () => {
    store.dispatch(actions.quitActivity())

    expect(store.getActions()[0].type).toBe('RESET')
    expect(store.getActions()[1].type).toBe('SET_ACTIVITY')
  })

  it('restartActivity', () => {
    store.dispatch(actions.restartActivity())

    expect(store.getActions()[0].type).toBe('RESET')
    expect(store.getActions()[1].type).toBe('START')
  })
})