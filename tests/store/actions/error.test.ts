import "jest"
import * as actions from '../../../src/store/actions/error'
import { ErrorAction } from '../../../src/interfaces'

describe('actions:error', () => {
  it('setError', () => {
    expect(actions.setError('Some Error')).toEqual({
      type: ErrorAction.SET_ERROR,
      payload: 'Some Error'
    })
  })
})