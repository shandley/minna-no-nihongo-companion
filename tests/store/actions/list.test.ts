import "jest"
import * as actions from '../../../src/store/actions/list'
import { ListAction } from '../../../src/interfaces'

describe('actions:list', () => {
  it('setAdjectives', () => {
    expect(actions.setAdjectives([])).toEqual({
      type: ListAction.SET_ADJECTIVES,
      payload: []
    })
  })

  it('resetAdjectives', () => {
    expect(actions.resetAdjectives()).toEqual({
      type: ListAction.RESET_ADJECTIVES
    })
  })

  it('setVerbs', () => {
    expect(actions.setVerbs([])).toEqual({
      type: ListAction.SET_VERBS,
      payload: []
    })
  })

  it('resetVerbs', () => {
    expect(actions.resetVerbs()).toEqual({
      type: ListAction.RESET_VERBS
    })
  })
})