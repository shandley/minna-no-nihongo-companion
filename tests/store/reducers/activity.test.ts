import 'jest'
import { ActivityAction as Actions, ActivityType } from '../../../src/interfaces'
import { initialState, reducer } from '../../../src/store/reducers/activity'
import { VerbConjugationTester } from '../../../src/activities/verbConjugationTester'
import { MockActivity } from '../../datamocks/activities'

describe('reducer:activity', () => {

  describe('SET_ACTIVITY', () => {
    it('sets an activity', () => {
      const activity = new VerbConjugationTester()

      const result = reducer(initialState, { type: Actions.SET_ACTIVITY, payload: activity })

      expect(result.currentActivity).toEqual(activity)
      expect(result.running).toEqual(false)
      expect(result.currentActivity.type).toBe(ActivityType.TEST_VERB_CONJUGATION)
    })
  })

  describe('RESET', () => {
    it('should reset the results on a reset', () => {
      const result = reducer(undefined, { type: Actions.RESET, payload: null })

      expect(result.results).toEqual([])
    })

    it('should not reset the options on a reset', () => {
      const nextState = reducer(undefined, {
        type: Actions.SET_OPTION,
        payload: {
          activityId: 1,
          optionKey: "MIN_CHAPTER",
          optionValue: 10
        }
      })

      const result = reducer(nextState, { type: Actions.RESET, payload: null })

      expect(result.options["1"]).toEqual({
        "MIN_CHAPTER": 10,
        "NUM_QUESTIONS": 10
      })
    })
  })

  describe('NEXT_QUESTION', () => {
    it('should immediately return if state.correct is null', () => {
      const nextState = reducer(initialState, { type: Actions.NEXT_QUESTION })
      expect(nextState).toEqual(initialState)
    })

    it('should setup the next question state if the answer has been evaluated', () => {
      const activity = new MockActivity()
      activity.setEvaluateAnswer(jest.fn().mockReturnValue({
        correct: true
      }))

      const setActivityState = reducer(initialState, {
        type: Actions.SET_ACTIVITY,
        payload: activity
      })

      const answerState = reducer(setActivityState, {
        type: Actions.SUBMIT_ANSWER,
        payload: ['foo']
      })

      const nextState = reducer(answerState, {
        type: Actions.NEXT_QUESTION
      })

      expect(nextState.correct).toBe(null)
    })
  })

  describe('COMPLETE', () => {
    it('should set running to false and complete to true when completing an activity', () => {
      const nextState = reducer(undefined, { type: Actions.COMPLETE })

      expect(nextState.complete).toEqual(true)
      expect(nextState.running).toEqual(false)
    })
  })

  describe('SET_OPTION', () => {
    it('can set a single option', () => {
      const state = reducer(undefined, {
        type: Actions.SET_OPTION,
        payload: {
          activityId: 1,
          optionKey: "MIN_CHAPTER",
          optionValue: 10
        }
      })

      expect(state.options["1"]).toEqual({
        "MIN_CHAPTER": 10,
        "NUM_QUESTIONS": 10
      })
    })

    it('can set an option and merge correctly', () => {
      const activityId = 1

      const state = reducer(undefined, {
        type: Actions.SET_OPTION,
        payload: {
          activityId,
          optionKey: "MIN_CHAPTER",
          optionValue: 14
        }
      })

      expect(state.options[activityId]).toEqual({
        "MIN_CHAPTER": 14,
        "NUM_QUESTIONS": 10
      })

      const nextState = reducer(state, {
        type: Actions.SET_OPTION,
        payload: {
          activityId,
          optionKey: "MAX_CHAPTER",
          optionValue: 18
        }
      })

      expect(nextState.options[activityId]).toEqual({
        "MIN_CHAPTER": 14,
        "MAX_CHAPTER": 18,
        "NUM_QUESTIONS": 10
      })
    })

    it('should request the correct number of questions from the currentActivity when the option has been set', () => {
      const activity = new VerbConjugationTester()
      const afterActivitySetState = reducer(initialState, { type: Actions.SET_ACTIVITY, payload: activity })

      expect(afterActivitySetState.currentActivity.type).toBe(ActivityType.TEST_VERB_CONJUGATION)

      const afterOptionsSetState = reducer(afterActivitySetState, {
        type: Actions.SET_OPTION,
        payload: {
          activityId: ActivityType.TEST_VERB_CONJUGATION,
          optionKey: "NUM_QUESTIONS",
          optionValue: 8
        }
      })

      spyOn(activity, 'getQuestions').and.returnValue([][Symbol.iterator]())

      reducer(afterOptionsSetState, { type: Actions.START })

      expect(activity.getQuestions).toHaveBeenCalledWith({
        NUM_QUESTIONS: 8
      })
    })
  })

  describe('SUBMIT_ANSWER', () => {
    it('should evaluate an answer via the current activity instance', () => {
      const activity = new MockActivity()
      activity.setEvaluateAnswer(jest.fn().mockReturnValue({
        correct: true
      }))

      const afterActivitySetState = reducer(initialState, {
        type: Actions.SET_ACTIVITY,
        payload: activity
      })

      const afterAnswerSubmitted = reducer(afterActivitySetState, {
        type: Actions.SUBMIT_ANSWER,
        payload: []
      })

      expect(activity.evaluateAnswer).toHaveBeenCalledWith([], {
        answer: [''],
        id: '',
        options: [],
        question: ''
      })

      expect(afterAnswerSubmitted.submittedAnswer).toEqual([])
      expect(afterAnswerSubmitted.correct).toBe(true)
    })
  })

  describe('SKIP_QUESTION', () => {
    it('should return the answer as false when skipping a question', () => {
      const activity = new MockActivity()

      const afterActivitySetState = reducer(initialState, { type: Actions.SET_ACTIVITY, payload: activity })

      const afterAnswerSubmitted = reducer(afterActivitySetState, {
        type: Actions.SKIP_QUESTION,
      })

      expect(afterAnswerSubmitted.submittedAnswer).toEqual([])
      expect(afterAnswerSubmitted.correct).toBe(false)
    })
  })

  describe('RECORD_RESULT', () => {
    it('should not return any feedback if the result is correct', () => {
      const activity = new MockActivity()
      activity.setEvaluateAnswer(jest.fn().mockReturnValue({
        correct: true
      }))

      const afterActivitySetState = reducer(initialState, {
        type: Actions.SET_ACTIVITY,
        payload: activity
      })

      const afterAnswerSubmitted = reducer(afterActivitySetState, {
        type: Actions.SUBMIT_ANSWER,
        payload: []
      })

      const afterRecordResult = reducer(afterAnswerSubmitted, {
        type: Actions.RECORD_RESULT
      })

      expect(afterRecordResult.results.length).toBe(1)
      expect(afterRecordResult.results[0].feedback).toBe(null)
    })

    it('should store the feedback result if the result is incorrect', () => {
      const activity = new MockActivity()
      
      activity.setEvaluateAnswer(jest.fn().mockReturnValue({
        correct: false
      }))

      const mockFeedback = {
        missing: 'blah',
        advice: 'git gud'
      }

      activity.setGetFeedback(jest.fn().mockReturnValue(mockFeedback))

      const afterActivitySetState = reducer(initialState, {
        type: Actions.SET_ACTIVITY,
        payload: activity
      })

      const afterAnswerSubmitted = reducer(afterActivitySetState, {
        type: Actions.SUBMIT_ANSWER,
        payload: []
      })

      const afterRecordResult = reducer(afterAnswerSubmitted, {
        type: Actions.RECORD_RESULT
      })

      expect(afterRecordResult.results.length).toBe(1)
      expect(afterRecordResult.results[0].feedback).toBe(mockFeedback)
    })
  })
})
