import 'jest'
import { ListAction as Actions } from '../../../src/interfaces/list'
import { reducer } from '../../../src/store/reducers/list'
import adjectives from 'minna-no-nihongo-adjectives'
import verbs from 'minna-no-nihongo-verbs'

describe('reducer:list', () => {
  it('sets a filtered list of verbs', () => {
    const result = reducer(undefined, { type: Actions.SET_VERBS, payload: [] })
    expect(result.filteredVerbs).toEqual([])
  })

  it('sets a filtered list of adjectives', () => {
    const result = reducer(undefined, { type: Actions.SET_ADJECTIVES, payload: [adjectives[0]] })
    expect(result.filteredAdjectives).toHaveLength(1)
  })

  it('resets verbs', () => {
    const result = reducer(undefined, { type: Actions.SET_VERBS, payload: [] })
    const nextState = reducer(result, { type: Actions.RESET_VERBS })

    expect(nextState.filteredVerbs).toEqual(verbs)
  })

  it('resets adjectives', () => {
    const result = reducer(undefined, { type: Actions.SET_ADJECTIVES, payload: [] })
    const nextState = reducer(result, { type: Actions.RESET_ADJECTIVES })

    expect(nextState.filteredAdjectives).toEqual(adjectives)
  })
})
