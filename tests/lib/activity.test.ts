import "jest"
import { ratePerformance } from '../../src/lib'

describe('lib:activity', () => {
  describe('ratePerformance', () => {
    it('returns the correct string for 100% result', () => {
      expect(ratePerformance(1, 1)).toEqual('Excellent! You scored 100%')
    })
  
    it('returns the correct string for 80% result', () => {
      expect(ratePerformance(10, 8)).toEqual('Nice. You scored 80%')
    })

    it('correctly rounds for 75% result', () => {
      expect(ratePerformance(4, 3)).toEqual('Not Bad... You scored 75%')
    })
  
    it('returns the correct string for 60% result', () => {
      expect(ratePerformance(10, 6)).toEqual('Not Bad... You scored 60%')
    })

    it('correctly rounds for 66.667% result', () => {
      expect(ratePerformance(3, 2)).toEqual('Not Bad... You scored 67%')
    })
  
    it('returns the correct string for 40% result', () => {
      expect(ratePerformance(10, 4)).toEqual('Hmmm... You scored 40%')
    })
  
    it('returns the correct string for 20% result', () => {
      expect(ratePerformance(10, 2)).toEqual('Bummer... You scored 20%')
    })
  
    it('returns the correct string for < 20% result', () => {
      expect(ratePerformance(10, 0)).toEqual('Oh dear... You scored 0%')
    })
  })
})
