import 'jest'
import * as kanji from '../../src/lib/kanji'

describe('lib:kanji', () => {
  describe('withRuby', () => {
    it('returns the original string when no kanji is found', () => {
      expect(kanji.withRuby('')).toBe('')
    })

    describe('when provided hiragana', () => {
      it('rubyizes known kanji', () => {
        expect(kanji.withRuby('わたし')).toBe('<ruby>私<rp>(</rp><rt>わたし</rt><rp>)</rp></ruby>')
        expect(kanji.withRuby('いきます')).toBe('<ruby>行<rp>(</rp><rt>い</rt><rp>)</rp></ruby>きます')
        expect(kanji.withRuby('きます')).toBe('<ruby>来<rp>(</rp><rt>き</rt><rp>)</rp></ruby>ます')
      })

      it('handles a whole sentence with a single match', () => {
        expect(kanji.withRuby(
          'わたし は がくせい です。B さん は'
        )).toBe(
          '<ruby>私<rp>(</rp><rt>わたし</rt><rp>)</rp></ruby> は がくせい です。B さん は'
        )
      })

      it('handles a whole sentence with 2 matches', () => {
        expect(kanji.withRuby(
          'わたし は おおさか へ いきます'
        )).toBe(
          '<ruby>私<rp>(</rp><rt>わたし</rt><rp>)</rp></ruby> は おおさか へ <ruby>行<rp>(</rp><rt>い</rt><rp>)</rp></ruby>きます'
        )
      })

      it('matches ほん', () => {
        expect(kanji.withRuby(
          'ほん を よんで いる'
        )).toBe(
          '<ruby>本<rp>(</rp><rt>ほん</rt><rp>)</rp></ruby> を よんで いる'
        )
      })
    })

    describe('when provided kanji', () => {
      it('matches a single kanji in a two-letter string', () => {
        expect(kanji.withRuby(
          '静か'
        )).toBe(
          '<ruby>静<rp>(</rp><rt>しず</rt><rp>)</rp></ruby>か'
        )
      })

      // it('matches two kanjis in a three-letter string', () => {
      //   expect(kanji.withRuby(
      //     '綺麗な'
      //   )).toBe(
      //     '<ruby>静<rp>(</rp><rt>しず</rt><rp>)</rp></ruby>か'
      //   )
      // })

      it('rubyizes a known kanji', () => {
        expect(kanji.withRuby('綺麗')).toBe(
          '<ruby>綺<rp>(</rp><rt>き</rt><rp>)</rp></ruby><ruby>麗<rp>(</rp><rt>れい</rt><rp>)</rp></ruby>'
        )
      })

      it('returns the original string when no kanji match is found', () => {
        expect(kanji.withRuby('馬刺')).toBe('馬刺')
      })
    })
  })
})