import 'jest'
import { collateFeedback } from '../../src/lib/feedback'
import { ICollatedFeedback } from '../../src/interfaces/storage'
import {
  correctItemWithFeedback,
  incorrectItemWithFeedback,
  incorrectItemWIthNoFeedback,
  incorrectItemWithMultipleIdenticalFeedbackItems,
  incorrectItemWithMultipleUniqueFeedbackItems,
} from './feedback.data'

describe('lib:feedback', () => {
  it('returns [] for an incorrect answer, where the question has no feedback', () => {
    expect(collateFeedback(incorrectItemWIthNoFeedback)).toEqual([])
  })

  it('returns [] if the answer is correct, despite the answer returning feedback', () => {
    expect(collateFeedback(correctItemWithFeedback)).toEqual([])
  })

  it('returns a single item for a single incorrect instance of a question with feedback', () => {
    expect(collateFeedback(incorrectItemWithFeedback) as ICollatedFeedback[]).toEqual([
      {
        result: {
          time: 567489326578432,
          correct: false,
          submittedAnswer: ['blah']
        },
        feedback: {
          missing: 'いく',
          advice: 'The plain form of いきます is いく'
        }
      }
    ])
  })

  it('does not duplicate feedback items when multiple results have he same feeback', () => {
    expect(collateFeedback(incorrectItemWithMultipleIdenticalFeedbackItems)).toEqual([
      {
        result: {
          time: 567489326578432,
          correct: false,
          submittedAnswer: ['blah']
        },
        feedback: {
          missing: 'いく',
          advice: 'The plain form of いきます is いく'
        }
      }
    ])
  })

  it('returns both items where > 1 feedback item is triggered by the answers', () => {
    expect(collateFeedback(incorrectItemWithMultipleUniqueFeedbackItems)).toEqual([
      {
        result: {
          time: 567489326578432,
          correct: false,
          submittedAnswer: ['いる']
        },
        feedback: {
          missing: 'かぶって',
          advice: 'The て-form of wearing (head) is かぶって'
        }
      },
      {
        result: {
          time: 567489326578432,
          correct: false,
          submittedAnswer: ['かぶって']
        },
        feedback: {
          missing: 'いる',
          advice: 'You need to say that they are wearing the item :/'
        }
      }
    ])
  })
})