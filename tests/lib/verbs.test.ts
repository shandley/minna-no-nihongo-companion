import 'jest'
import { getVerbConjugationQuestions} from '../../src/lib'
import { IVerbConjugationTesterOptions, VerbType } from '../../src/interfaces'

describe('lib:verbs', () => {
  const options: IVerbConjugationTesterOptions = {
    NUM_QUESTIONS: 10,
    ANSWER_VERB_FORM: VerbType.PLAIN,
    QUESTION_VERB_FORM: VerbType.POLITE
  }

  describe('getVerbConjugationQuestions', () => {
    it('correctly fetches 10 questions', () => {
      expect(getVerbConjugationQuestions(options)).toHaveLength(10)
    })
  })
})