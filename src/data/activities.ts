import { IActivity } from '../interfaces'
import {
  AdjectiveDictionary,
  AdjectiveTester,
  GrammarTester,
  SentenceTester,
  VerbConjugationTester,
  VerbDictionary,
} from '../activities'

export const activities: IActivity[] = [
  new GrammarTester(),
  new SentenceTester(),
  new AdjectiveTester(),
  new VerbConjugationTester(),
  new AdjectiveDictionary(),
  new VerbDictionary()
]
