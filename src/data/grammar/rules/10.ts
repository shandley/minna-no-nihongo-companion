import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.YA_LIST_ITEMS,
    particles: [
      'や'
    ],
    rule: '[N] や [N] や ... など ...',
    explanation:
      `や is used to link together several items in an incomplete list (followed by など)`
  },
] as IGrammarRule[]
