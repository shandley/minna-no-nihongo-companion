import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    particles: [
      'こと'
    ],
    rule: '[V.TaForm] こと が あります',
    explanation:
      `This sentence pattern is used to say what happened in the past as a particular experience`
  },
  {
    id: Keys.TA_FORM_LIST,
    particles: [
      'り'
    ],
    rule: '[V1.TaForm]り、[V2.TaForm]り、[VN...]り します',
    explanation:
      `When listing verbs, や is replaced with り attached to the verb.
       Tense is declared at the end of the sentence.`
  },
  {
    id: Keys.BECOME_II_ADJ_KU,
    particles: [
      'く'
    ],
    rule: 'い-Adjective [-い] [+く] なります',
    explanation:
      `This pattern allows describing a change in state. When the condition is stated using an い-Adjective, 
      the い is dropped and replaced with 'く' followed by 'なります'`
  },
  {
    id: Keys.BECOME_NA_ADJ_NI,
    particles: [
      'に'
    ],
    rule: 'な-Adjective [-な] [+に] なります',
    explanation:
      `This pattern allows describing a change in state. When the condition is stated using an な-Adjective, 
      the な is dropped and replaced with 'に' followed by 'なります'`
  },
  {
    id: Keys.BECOME_NOUN_NI,
    particles: [
      'に'
    ],
    rule: '[Noun] に なります',
    explanation:
      `This pattern allows describing a change in state. When the condition is stated using a noun, 
      it is followed by 'に なります'`
  },
] as IGrammarRule[]
