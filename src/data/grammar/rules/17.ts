import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.NAI_FORM_POLITE_REQUEST,
    particles: [
      'ない',
      'で'
    ],
    rule: '[V.NaiForm] ない で ください',
    explanation: 'This format is used to ask or tell someone not to do something in a polite way'
  },
  {
    id: Keys.NAI_FORM_PLEASE_DONT,
    particles: [
      'ない',
      'で'
    ],
    rule: '[V.NaiForm] ない で ください',
    explanation:
      `This sentence pattern is used to tell something that an action described by the verb 
      is not required.`
  },
  {
    id: Keys.NAI_FORM_MUST_DO,
    particles: [
      'なければ',
    ],
    rule: '[V.NaiForm] なければ なりません',
    explanation:
      `This sentence pattern is used to say that something must be done.`
  },
  {
    id: Keys.NAI_FORM_OPTIONAL,
    particles: [
      'なくて',
      'も'
    ],
    rule: '[V.NaiForm] なくて も いいです',
    explanation:
      `This sentence pattern is used when sayig that an action is not necessary`
  },
  {
    id: Keys.MADE_MUST_BE_COMPLETED_BY,
    particles:[
      'まで'
    ],
    rule: '[N] Time までに [V]',
    explanation: 
      `This pattern is used to indicate that an event or action must be performed or completed 
      by the specified Time.`
  },
] as IGrammarRule[]
