import { IGrammarRule } from '../../../interfaces'
import { Keys } from '../keys'

export default [
  {
    id: Keys.HOSHII,
    particles: [
      '欲しい'
    ],
    rule: '[N] が ほしい です',
    explanation:
      `Describes the speaker wanting the preceeding noun. ほしい is an い-adj and is always followed by が`
  },
] as IGrammarRule[]
