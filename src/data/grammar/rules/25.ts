import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.VERB_TA_RA_IF,
    particles: [
      'ら'
    ],
    rule: '[V.TaForm] [condition] ... [action]',
    explanation:
      `When 'た' is added after a past tense た verb, it indicates a conditional statement in the form
      '[if] condition [then] action'. This structure can be used to express a want or hope, which is dependent on 
      something being true before it can be acieved.`
  },
  {
    id: Keys.VERB_TA_RA_WHEN_AFTER_ONCE,
    particles: [
      'ら'
    ],
    rule: '...[V.TaForm] ら [...clause]',
    explanation:
      `This pattern allows describing that an action will occur after/once/when another action completes. For 
      example, speaking to a colleague once you arrive at work, or cooking dinner after you return home`
  },
  {
    id: Keys.VERB_TE_MO,
    particles: [
      'も'
    ],
    rule: '...[V.TeForm] も [...activity]',
    explanation:
      `This patterns allows describing an 'even if...' scenario where the conditional is a Verb. For example, Even if
      it rains (rain falls) I will go to the park.`
  },
  {
    id: Keys.VERB_NAI_NA_KU_TE_MO,
    particles: [
      'も'
    ],
    rule: '...[V.NaiForm] くて も [...clause]',
    explanation:
      `This pattern describes a scenarion where a negative condition explains the clause which follows. For example
      'I dont like it, even if it is cheap: やすい くて も、すき じゃない.`
  },
  {
    id: Keys.NOUN_DE_MO,
    particles: [
      'も'
    ],
    rule: '[N] で も [action]',
    explanation:
      `This variant of the も pattern is used when the 'even if' conditional expression is a Noun,
      for example, a day of the week.`
  },
  {
    id: Keys.II_ADJ_KU_TE_MO,
    particles: [
      'も'
    ],
    rule: '[い-Adj] くて も [...clause]',
    explanation:
      `This variant of the も pattern is used when the 'even if' conditional expression is an い-adjective,
      for example, a day of the week.`
  },
  {
    id: Keys.NA_ADJ_DE_MO,
    particles: [
      'も'
    ],
    rule: '[な-Adj] くて も [...clause]',
    explanation:
      `This variant of the も pattern is used when the 'even if' conditional expression is a な-adjective.`
  },
] as IGrammarRule[]
