import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.PLAIN_FORM_SENTENCE_GA,
    particles: [
      'が'
    ],
    rule: '...が',
    explanation:
      `Be careful about the topic of the sentence, which will be denoted by は. References to the subject should be が`
  },
  {
    id: Keys.PLAIN_FORM_SENTENCE_WO,
    particles: [
      'を'
    ],
    rule: 'Mystery :(',
    explanation: 'I have NFI'
  },
  {
    id: Keys.PLAIN_FORM_SENTENCE_WA,
    particles: [
      'は'
    ],
    rule: 'Mystery :(',
    explanation: 'I have NFI'
  },
  {
    id: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    particles: [],
    rule: '[Adjective] [V.PlainForm] [Noun]',
    explanation:
      `Verbs, adjectives and nouns in this format are in Plain Form. This pattern is used to describe
       something by how it was made, or what it does. For example, a person who does [V], a cake that was made
       by [Person], etc...Generally the clause coming before the Noun. When い adjectives are listed, they use て while
       な adjectives use な. Nouns use の.`
  },
  {
    id: Keys.TA_FORM_VERB_AT_PLACE,
    particles: [
      'で'
    ],
    rule: '[Place] で [Verb.TaForm] [Noun] です',
    explanation:
      `When describing a [Verb] at some [Place] you can use '[Place] で [V.TaForm] [Noun] です.
       For example, saying that you took a photo at the library would be 'としょうかん で とりました です'.
       To extend this to 'I took *this* photo at the library' the noun must be specified (the photo).
       This form allows this: the example becomes 'としょうかん で とった しゃしん です'`
  },
  {
    id: Keys.CANT_DO_YAKUSOKU,
    particles: [],
    rule: 'いいえ [Noun] [Particle] [V.DictionaryForm], やくそく が あります',
    explanation:
      `Used when declining an invitation to participate in an activity because you already have plans.
      For example, if you have to go to work, 'Go' is the verb, and 'Work' is the Noun: 
      'いいえ しごと へ いる, やくそく が あります'. Adjust N/Particle/V as approprite, but keep 
      the Verb in dictinary form`
  },
  {
    id: Keys.CANT_DO_JIKAN_GA_ARIMASEN,
    particles: [
      'が'
    ],
    rule: 'いいえ [Noun] [Particle] [V.DictionaryForm], じかん が ありません (でした)',
    explanation:
      `Used when declining an invitation to participate in an activity because you don't have time.
       For example, if you have to go to work, 'Go' is the verb, and 'Work' is the Noun: 
       'いいえ しごと へ いる, じかん が ありません'. Adjust N/Particle/V as approprite, but keep 
       the Verb in dictinary form`
  },
] as IGrammarRule[]
