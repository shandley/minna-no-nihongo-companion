import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.NO_SUBJECT,
    particles: [
      'の'
    ],
    rule: '[N.1] の [N.2]',
    explanation:
      `In this sentence form, [N.1] indicates the subject of [N.2]. A coffe book would be コーヒー の ほん`
  },
] as IGrammarRule[]
