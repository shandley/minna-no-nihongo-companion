import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.MISC,
    particles: [],
    rule: 'N/A',
    explanation:
      `This is a general question to test vocabulary`
  }
] as IGrammarRule[]
