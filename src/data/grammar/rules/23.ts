import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.TOKI_II_ADJ,
    particles: [
      'とき'
    ],
    decoys: [
      'の とき',
      'な とき',
      'から'
    ],
    rule: '[いい-Adj] とき [...clause]',
    explanation:
      `In this とき form, use the いい-adjective followed by とき.`
  },
  {
    id: Keys.TOKI_NA_ADJ,
    particles: [
      'とき'
    ],
    rule: '[な-Adj] な とき [...clause]',
    explanation:
      `In this とき form, the な-adjective includes the な suffix followed by とき.`
  },
  {
    id: Keys.TOKI_NOUN,
    particles: [
      'の'
    ],
    rule: '[N] の とき [...clause]',
    explanation:
      `When the とき form is prepended with a Noun, it is necessary to add の before とき`
  },
  {
    id: Keys.TOKI_VERB_DICTIONARY_FORM,
    particles: [
      'とき'
    ],
    decoys: [
      'の とき',
      'な とき',
      'から'
    ],
    rule: '[V.DictForm (or ない Form)] とき [...clause]',
    explanation:
      `The Verb should be in Dictionary Form followed by とき. The same holds true for the ない form of the verb`
  },
  {
    id: Keys.TOKI_VERB_AT_TIME,
    particles: [
      'とき'
    ],
    rule: '[V.DictForm or V.TaForm] とき [...clause]',
    explanation:
      `The verb form used with とき determines the time at which the action occurred. Using the dictionary 
      form of the verb implies that the action occured before the clause, while using the た-form indicates 
      that the action occurred during clause. For example, when travelling to a location, this differentiates 
      an action before leaving (る) vs. an action while at the location (た)`
  },
  {
    id: Keys.TO_IF_THEN,
    particles: [
      'と'
    ],
    rule: '[V.DictionaryForm] と [...clause]',
    explanation:
      `Using と with a dictionary verb in this form describes a scenario where if an action is performed, a resultant
      action or outcome will occur.`
  },
  {
    id: Keys.NOUN_WO_MOTION_VERB,
    particles: [
      'を'
    ],
    rule: '[N] を [V.Motion]',
    explanation:
      `In certain verb cases involving movement, を is used as the particle. These cases generally
      involve the movement 'through' the location, such as a park, an intersection, or across a road
      or bridge. These locations are generally not destinations, so に/へ do not apply.`
  },
] as IGrammarRule[]
