import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.NOUN_DONNA,
    particles: [
      'どんな'
    ],
    rule: '[N.1] は どんな [N.2] です か？',
    explanation:
      `This structure is used when asking about a noun in the form of 'what kind of N2 is N1'. 
      It accomodates asking about the nature of the noun, such as 'WHat kind of person is Aさん?'`
  },
  {
    id: Keys.DOU_DESU_KA,
    particles: [
      'どう'
    ],
    rule: '[N] は どう です か？',
    explanation:
      `This structure is used to ask a person about their impression of the Noun. It accomodates asking
      about how they feel about N, where N can be a place, person, thing...etc that they have experienced.`
  },
] as IGrammarRule[]
