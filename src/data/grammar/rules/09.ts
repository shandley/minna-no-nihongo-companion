import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.DONNA_NOUN,
    particles: [
      'どんな'
    ],
    rule: 'どんな [N]',
    explanation:
      `This structure is used when asking something in the for 'which one?'. The clause following this
      statement defines what characteristc is being asked about. For example, 'Which is the most interesting city'`
  },
  {
    id: Keys.YOKU,
    particles: [
      'よく'
    ],
    rule: 'よく [V.Positive]',
    explanation:
      `A positive adverb (degree: strong).`
  },
  {
    id: Keys.DAITAI,
    particles: [
      'だいたい'
    ],
    rule: 'だいたい [V.Positive]',
    explanation:
      `A positive adverb (degree: moderately positive).`
  },
  {
    id: Keys.TAKUSAN,
    particles: [
      'たくさん'
    ],
    rule: 'たくさん [N]',
    explanation:
      `An adverb (quantity: strong).`
  },
  {
    id: Keys.SUKOSHI,
    particles: [
      'すこし'
    ],
    rule: 'すこし [A] || [V]',
    explanation:
      `An adverb (posiive degree, slight).`
  },
  {
    id: Keys.AMARI,
    particles: [
      'あまり'
    ],
    rule: 'あまり [V]',
    explanation:
      `A negative adverb (negative degree or quantity, slight).`
  },
  {
    id: Keys.ZENZEN,
    particles: [
      'ぜんぜん'
    ],
    rule: 'ぜんぜん [V.Negative]',
    explanation:
      `A negative adverb (negative degree or quantity, strong).`
  },
] as IGrammarRule[]
