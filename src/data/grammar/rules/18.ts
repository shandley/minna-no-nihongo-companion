import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.DEKIMASU_NOUN,
    particles: [
      'が'
    ],
    rule: '[N] が できます',
    explanation:
      `Nouns indicating motion or nouns reprenting an ability can be expressed with が できます`
  },
  {
    id: Keys.DEKIMASU_VERB,
    particles: [
      'が',
      'こと'
    ],
    rule: '[V.DictionaryForm] こと が できます',
    explanation:
      `When a verb is used to describe an ability or possibility, こと is attached to the verb's 
      dictionary form to make it a noun phrase, which is then followed by が できます. こと basically
      means 'event' or 'occurance'.`,
  },
  {
    id: Keys.HOBBIES_NOUN_FORM,
    particles: [
      'は',
    ],
    rule: '私の しゅみ は [N] です',
    explanation:
      `This structure is used when a person wants to declare a hobby as a simple noun`
  },
  {
    id: Keys.HOBBIES_VERB_FORM,
    particles: [
      'を',
      'こと'
    ],
    rule: '私の しゅみ は [N] を [V.DictionaryForm] こと です',
    explanation:
      `This structure is used when a person wants to declare a hobby while including a noun and a verb.`
  },
  {
    id: Keys.MAE_NI_VERB,
    particles: [
      'まえ',
    ],
    rule: '[V.DictionaryForm] まえ に ...',
    explanation:
      `This structure is used to declare that some action was done before the appended statement. 
      The verb is used in it's dictionary form followed by まえ に. For example, I bought a book before coming to 
      work: しごと へ くる まえ に, ほん を かいました`
  },
  {
    id: Keys.MAE_NI_NOUN_NO,
    particles: [
      'まえ'
    ],
    rule: '[N] の まえ に ...',
    explanation:
      `This structure is used to declare that before [N] some action occurs. For example:
      I wash my hands before having breakfast: あさごはん の まえ に, て を あらいます`
  },
  {
    id: Keys.MAE_NI_TIME,
    particles: [
      'まえ'
    ],
    rule: '[Time] まえ に ...',
    explanation:
      `This structure is used to specify a time period before declaring a state. For example:
      I ate breakfast one hour ago: １じかん まえ に あさごはん を たべました`
  },
  {
    id: Keys.NAKANAKA,
    particles: [
      'なかなか'
    ],
    rule: 'なかなか [...clause]',
    explanation:
      `When なかなか is used with a negative expression, it can be used to describe something as difficult or rare`
  },
  {
    id: Keys.ZEHI,
    particles: [
      'ぜひ'
    ],
    rule: '[ぜひ] ...expression',
    explanation:
      `ぜひ is used to express strong desire for something. ぜひ can be prepended to a sentence to increase the speakers
      hope that the event will occur, such as an invitation, or using a verb in たい form`
  },
] as IGrammarRule[]
