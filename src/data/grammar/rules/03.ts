import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.DOCO_DOCHIRA,
    particles: [
      'どちら',
      'どこ'
    ],
    rule: '... [どこ / どちら] ...',
    explanation:
      `These particles are used in questions to ask about a location or direction. 'どちら' is a the more formal variant
       and is often used to specify 'which direction' however both are valid particles for 'where?' questions`
  },
] as IGrammarRule[]
