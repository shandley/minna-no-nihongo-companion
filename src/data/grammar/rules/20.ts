import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    particles: [],
    rule: '[V.TaForm] [-ます]',
    explanation:
      `Changing polite V-ます-Form to plain-form is to use the Ta-Form of the verb and omit the 'ます' entirely`
  },
  {
    id: Keys.PLAIN_FORM_VERB_CONVERSION_MASEN,
    particles: [
      'ない'
    ],
    rule: '[V.TaForm] [-ません] [+ない]',
    explanation:
      `Changing polite V-ません-Form to plain-form: Use the Ta-Form of the verb, omit the 'ます' and add 'ない'`
  },
  {
    id: Keys.PLAIN_FORM_VERB_CONVERSION_JYA_ARIMASEN_DESHITA,
    particles: [
      'ない'
    ],
    rule: '[V.TaForm] [-ありませんでした] [ない]',
    explanation:
      `Changing polite V-ありませんでした to plain-form: Use the Ta-Form of the verb, omit the
       'ありませんでした' and add 'ない' (じゃない)`
  },
  {
    id: Keys.PLAIN_FORM_VERB_CONVERSION_IMASU,
    particles: [
      'る'
    ],
    rule: '[V.TaForm] [-ます] [+る]',
    explanation:
      `To Change polite V-います-Form to plain-form, use the た-Form of the verb, omit the 'ます' and add 'る'`
  },
  {
    id: Keys.PLAIN_FORM_VERB_CONVERSION_MASHITA,
    particles: [
      'た'
    ],
    rule: '[V.TaForm] [-ました] [+た]',
    explanation:
      `To Change polite V-ました-Form to plain-form, use the た-Form of the verb, omit the 'ました' and add 'た'`
  },
  {
    id: Keys.PLAIN_FORM_VERB_CONVERSION_MASEN_DESHITA,
    particles: [
      'かって'
    ],
    rule: '[V.TaForm] [-ませんでした] [+なかった]',
    explanation:
      `To Change polite V-ました-Form to plain-form, use the Ta-Form of the verb, omit 'ませんでした' and add 'なかった'`
  },
  {
    id: Keys.PLAIN_FORM_CONVERSION_NOUN,
    particles: ['だ'],
    rule: '[N] [-です] [+だ]',
    explanation:
      `The plain form of 'です' is 'だ', so to change a Noun from polite to plain-form replace 'です' with 'だ'`
  },
  {
    id: Keys.PLAIN_FORM,
    particles: [],
    rule: 'Sentence with [V.DictionaryForm] ... without [です]',
    explanation:
      `In plain form, verbs and adjectives use the dictionary form, and 'です/ます' suffixes are omitted.`
  },
  {
    id: Keys.PLAIN_FORM_QUESTIONS_DROP_KA,
    particles: [],
    rule: 'Sentence [-か]',
    explanation:
      `Plain-form sentences generally omit 'か' from the end of questions. Instead, rising intonation 
       is used to indicate that the sentence is a question.`
  },
  {
    id: Keys.PLAIN_FORM_QUESTIONS_DROP_DA,
    particles: [
      'うん'
    ],
    rule: '[-はい] [+うん] / [-いいえ] [+ううん]',
    explanation:
      `The answer to plain form questions replaced 'はい' with 'うん' and 'いいえ' with 'ううん'.
       The 'だ' is also ommitted (だ is the plain form of です)`
  },
] as IGrammarRule[]
