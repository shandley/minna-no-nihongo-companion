import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.TE_FORM_MAY_I,
    particles: [
      'も'
    ],
    rule: '[V.TeForm] も いいですか',
    explanation:
      `This structure is used to ask permission, or to confirm whether an action is acceptable`
  },
  {
    id: Keys.TE_FORM_MUST_NOT_DO,
    particles: [
      'は'
    ],
    rule: '[V.TeForm] は いけません',
    explanation:
      `This structure is used to communicate that an action is not allowed`
  },
  {
    id: Keys.TE_FORM_CURRENT_STATE,
    particles: [
      'います'
    ],
    rule: '[V.TeForm] います',
    explanation:
      `This structure describes an activity which is not temporary, such as being married, 
      or knowing a person. If you use a noun (such as marriage) remember to add a verb (して) so that you do not declare
      that you are the noun. For example, わたし は けっこん います (I am a marriage) vs. けっこん して います (am married)`
  },
] as IGrammarRule[]
