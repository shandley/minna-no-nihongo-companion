import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.STATING_OPINION,
    particles: [
      'と'
    ],
    rule: '[...statement] と おもいます',
    explanation:
      `When 'と おもいます' is used following a statement, it communicates that the statement is an opinion.`
  },
  {
    id: Keys.ASKING_OPINION,
    particles: [
      'どう'
    ],
    rule: '[...clause] どう おもいます か？',
    explanation:
      `This pattern is used to ask another person their opinion about something.`
  },
  {
    id: Keys.OPINION_AGREEMENT,
    particles: [
      'そう'
    ],
    rule: 'わたし も そう おもいます',
    explanation:
      `This sentence form is used when you agree witha stated opinion`
  },
  {
    id: Keys.MAYBE_DEFINITELY,
    particles: [
      'たぶん',
      'きっと'
    ],
    rule: '[たぶん] || [きった] ...',
    explanation:
      `たぶん and きっと are adverbs which can be used to vary the strength of a thought opinion.
      たぶん softens, meaning 'probably' or 'maybe' while 'きっと' is used to mean the thought is closer 
      to 'definitely' or having greater than 90% confidence`
  },
] as IGrammarRule[]
