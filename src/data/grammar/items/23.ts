import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'cAOCEnUv2',
    ruleId: Keys.TOKI_VERB_DICTIONARY_FORM,
    hiragana: 'つま が びょうき の {とき} かいしゃ を やすみます',
    english: 'When my wife is Ill, I take time off work'
  },
  {
    id: 'E4DVDCQ-Y',
    ruleId: Keys.TOKI_II_ADJ,
    hiragana: 'あたま が いたい {とき} この くすり を のみます',
    english: 'When I have a headache, I take medicine'
  },
  {
    id: '6lbU2Zqep',
    ruleId: Keys.NOUN_WO_MOTION_VERB,
    hiragana: 'この こうさてん {を} みぎ へ まがります',
    english: 'Turn right at that intersection'
  },
] as IGrammarItem[]