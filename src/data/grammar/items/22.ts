import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'criqR6C5s',
    ruleId: Keys.PLAIN_FORM_SENTENCE_GA,
    hiragana: 'これ は わたし {が} かいて てがみ です',
    english: 'This is the letter I wrote'
  },
  {
    id: 'P2TVvXVMG',
    ruleId: Keys.PLAIN_FORM_SENTENCE_GA,
    hiragana: 'つま {が} つくって りょうり は おいしい です',
    english: 'The food my wife cooks is delicious'
  },
  {
    id: '4QuQCjmrR',
    ruleId: Keys.PLAIN_FORM_SENTENCE_WO,
    hiragana: 'ここ は くつ {を} ぬぐ ところ です',
    english: 'This area is where you remove your shoes'
  },
  {
    id: 'QM-g8upqv',
    ruleId: Keys.PLAIN_FORM_SENTENCE_WO,
    hiragana: 'きのう わたしは かのじょ に あげる くつ {を} かいました',
    english: 'Yesterday I bought shoes to give to my girlfriend',
  },
] as IGrammarItem[]