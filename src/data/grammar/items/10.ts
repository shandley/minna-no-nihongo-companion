import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: '6Kr2Lb_1pL',
    ruleId: Keys.YA_LIST_ITEMS,
    hiragana: 'コーヒー {や} パン {や} ぎゅうにゅう など かいまして',
    answer: ['や', 'や'],
    english: 'I bought coffee, bread, milk, etc...'
  },
] as IGrammarItem[]