import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'rY1CPcGcx7',
    ruleId: Keys.HE_DESTINATION,
    hiragana: 'とうきょう {へ} いきます',
    english: 'I will go to Tokyo'
  },
  {
    id: '2q_bd1R_sB',
    ruleId: Keys.HE_DESTINATION,
    hiragana: 'おおさか {へ} いきました',
    english: 'I went to Tokyo '
  },
  {
    id: 'HWX04naKT0',
    ruleId: Keys.HE_DESTINATION,
    hiragana: 'うち {へ} いきました',
    english: 'I went home'
  },
  {
    id: 'gejTL2uUGK8',
    ruleId: Keys.HE_DESTINATION,
    hiragana: [
      {
        line: 'なに を しました か',
      },
      {
        line: 'おおさか {へ} いきました',
      }
    ]
  },
  {
    id: 'eyC100VycBV',
    ruleId: Keys.DE_TRANSPORT_METHOD,
    hiragana: 'でんしゃ {で} いきます',
    english: 'I travelled by train'
  },
  {
    id: '9enGRd2ESL9',
    ruleId: Keys.TO_ACTION_TOGETHER,
    hiragana: 'かぞく {と} 日本 へ きました',
    english: 'I came to Japan with my family'
  },
  {
    id: 'lrapobBG9D8',
    ruleId: Keys.TO_ACTION_TOGETHER,
    hiragana: 'ともだち {と} えいが を みました',
    english: 'I went to a movie with my friend'
  },
  {
    id: '0KL3u4S7cN5',
    ruleId: Keys.MO_DENY_INTERROGATIVE,
    hiragana: [
      {
        line: 'どこ へ いきます か',
      },
      {
        line: 'どこ {も} いきません',
      }
    ]
  },
  {
    id: 'xUssgMC9Z6L',
    ruleId: Keys.MO_DENY_INTERROGATIVE,
    hiragana: [
      {
        line: 'なに を たべます か',
      },
      {
        line: 'なに {も} たべません',
      }
    ]
  },
  {
    id: 'CQohLhMaR7p',
    ruleId: Keys.MO_DENY_INTERROGATIVE,
    hiragana: 'だれ {も} いきません でした',
  },
] as IGrammarItem[]
