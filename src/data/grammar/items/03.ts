import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'v0eCzPNua',
    ruleId: Keys.DOCO_DOCHIRA,
    hiragana: 'おくに は {どちら} です か',
    english: 'Which country are you from?',
    feedback: [
      {
        match: 'どこ',
        advice: 'Use どちら for this introductory question (more polite)'
      }
    ]
  }
] as IGrammarItem[]