import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'izx52iYZmB',
    ruleId: Keys.NO_OWNERSHIP,
    hiragana: 'わたし {の} たんじょうび です',
    english: 'It\'s my birthday'
  },
  {
    id: 'cUAhNs_qKK',
    ruleId: Keys.NO_OWNERSHIP,
    hiragana: 'せんせい {の} くるま です',
    english: 'The teacher\'s car'
  },
  {
    id: 'jV4xjxMtz2',
    ruleId: Keys.NO_OWNERSHIP,
    hiragana: [
      {
        line: 'この かぎ は だれ の ですか',
      },
      {
        line: 'わたし {の} です',
      }
    ]
  },
  {
    id: '8PDe3mk0ELD',
    ruleId: Keys.MO_ALSO,
    hiragana: 'わたし {も} ねむい です',
    english: 'I\m also tired'
  },
  {
    id: 'HMdnYwlclSt',
    ruleId: Keys.MO_ALSO,
    hiragana: [
      {
        line: 'わたし は がくせい です。B さん は',
      },
      {
        line: 'あなた {も} がくせい です',
      }
    ]
  },
] as IGrammarItem[]