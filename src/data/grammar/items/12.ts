import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: '_JfNxRJ8w_p',
    ruleId: Keys.DE_CATEGORY,
    hiragana: 'くだもの {で} りんご が いちばん すき です',
    english: 'Which fruit do you enjoy most?'
  },
  {
    id: '7nhjapIamrO',
    ruleId: Keys.DE_CATEGORY,
    hiragana: [
      {
        line: 'きせつ {で} いつが いちばん すき です か',
      },
      {
        line: 'なつ が いちばん 好き です',
      }
    ]
  },
  {
    id: 'C132ab5dA',
    ruleId: Keys.YORI_COMPARISON,
    hiragana: 'この くるま は その くるま {より} おおきい です',
    english: 'This car is bigger than that car'
  },
] as IGrammarItem[]