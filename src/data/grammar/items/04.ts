import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'rSqWZZiffr',
    ruleId: Keys.TO_COORDINATE_RELATION,
    hiragana: 'ぎんこう の やすみ は どようび {と} にちようび です',
    english: 'The bank is closed on Saturday and Sunday'
  },
  {
    id: '5hmvufNCzX',
    ruleId: Keys.TO_COORDINATE_RELATION,
    hiragana: [
      {
        line: 'なに を かいました か？',
      },
      {
        line: 'ぱん {と} たまご が かいました',
      }
    ]
  },
  {
    id: 'P1J9sKb6G',
    ruleId: Keys.NI_TIME,
    hiragana: '6 じかん {に} おきます',
    english: 'I will wake up at 6'
  },
  {
    id: 'r3totL9JPzz',
    ruleId: Keys.MADE_KARA_BEGIN_END,
    hiragana: 'うち {から} えき {まで}',
    english: 'From my house to the train station'
  },
  {
    id: 'Cs8bmIUAP42',
    ruleId: Keys.MADE_KARA_BEGIN_END,
    hiragana: '1 じ {から} 5 じ {まで} べんきょう します'
  },
  {
    id: '_nLyu8La94K',
    ruleId: Keys.MADE_KARA_BEGIN_END,
    hiragana: 'とうきょう {から} おおさか {まで} ３じかん かかります',
    english: 'From Tokyo to Osaka takes 3 hours in total'
  },
] as IGrammarItem[]

