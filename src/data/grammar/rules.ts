import { IGrammarRule } from '../../interfaces'

import { default as chapter00 } from './rules/00'
import { default as chapter01 } from './rules/01'
import { default as chapter02 } from './rules/02'
import { default as chapter03 } from './rules/03'
import { default as chapter04 } from './rules/04'
import { default as chapter05 } from './rules/05'
import { default as chapter06 } from './rules/06'
import { default as chapter08 } from './rules/08'
import { default as chapter09 } from './rules/09'
import { default as chapter10 } from './rules/10'
import { default as chapter12 } from './rules/12'
import { default as chapter13 } from './rules/13'
import { default as chapter14 } from './rules/14'
import { default as chapter15 } from './rules/15'
import { default as chapter16 } from './rules/16'
import { default as chapter17 } from './rules/17'
import { default as chapter18 } from './rules/18'
import { default as chapter19 } from './rules/19'
import { default as chapter20 } from './rules/20'
import { default as chapter21 } from './rules/21'
import { default as chapter22 } from './rules/22'
import { default as chapter23 } from './rules/23'
import { default as chapter24 } from './rules/24'
import { default as chapter25 } from './rules/25'

export const grammarRules: IGrammarRule[] = [
  ...chapter00,
  ...chapter01,
  ...chapter02,
  ...chapter03,
  ...chapter04,
  ...chapter05,
  ...chapter06,
  ...chapter08,
  ...chapter09,
  ...chapter10,
  ...chapter12,
  ...chapter13,
  ...chapter14,
  ...chapter15,
  ...chapter16,
  ...chapter17,
  ...chapter18,
  ...chapter19,
  ...chapter20,
  ...chapter21,
  ...chapter22,
  ...chapter23,
  ...chapter24,
  ...chapter25,
]