import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'izx52iYZmB',
    ruleId: Keys.NO_OWNERSHIP,
    english: 'It\'s my {noun}',
    hiragana: 'わたし の {noun} です',
    feedback: [
      {
        missing: 'の',
        advice: 'Use the `の` particle to denote ownership'
      }
    ]
  },
  {
    id: 'Kv2iBIYYe',
    ruleId: Keys.NO_OWNERSHIP,
    english: 'Whose {noun} is that?',
    hiragana: 'だれ の {noun} です か',
    feedback: [
      {
        missing: 'の',
        advice: 'Use the `の` particle to denote ownership'
      }
    ]
  },
] as ISentence[]