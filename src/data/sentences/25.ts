import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'gxAaLLmO9',
    ruleId: Keys.VERB_TA_RA_IF,
    hiragana: 'おかね が あったら, りょうこ を します',
    english: 'If I had some money, I\'d go travelling',
    feedback: [
      {
        match: 'りょうこをします',
        advice: 'In this case, use `...が あったら` as we are talking about possession, not an action'
      }
    ]
  },
  {
    id: '0yGVqPEEQ',
    ruleId: Keys.VERB_TA_RA_IF,
    hiragana: 'あした あめ が ふったら, うち で べんきょう します',
    english: 'If it rains tomorrow, I will study at home'
  },
  {
    id: 'SW8Iu1NlO',
    ruleId: Keys.VERB_TA_RA_IF,
    hiragana: 'としょかん に いったら、この ほん を かえって ください',
    english: 'If you go to the library, please return this book',
    feedback: [
      {
        missing: 'かえって',
        advice: 'The て-form of return is `かえって`'
      }
    ]
  },
  {
    id: 'MFt9LWgBM',
    ruleId: Keys.VERB_TA_RA_IF,
    hiragana: 'でんしゃ が こなかったら, どう します か？',
    english: 'If the train doesn\'t come, what should we do?',
    feedback: [
      {
        match: 'でんしゃを',
        advice: 'Use `が` in this case'
      },
      {
        missing: 'こなかったら',
        advice: 'The verb for `くる` (come)) becomes `こなかったら` in a negative た-form'
      }
    ]
  },
  {
    id: 'EQ4Jg62V5',
    ruleId: Keys.VERB_TA_RA_WHEN_AFTER_ONCE,
    hiragana: 'ひるごはん を たべたら えいが を みに いきませんか？',
    english: 'After we eat lunch, do you want to watch a movie?',
    feedback: [
      {
        missing: 'いきません',
        advice: 'Use `いきません` to suggest going somewhere'
      }
    ]
  },
  {
    id: 'pjchaoBjS',
    ruleId: Keys.VERB_TA_RA_WHEN_AFTER_ONCE,
    hiragana: 'えき に ついたら, でんわ を ください',
    english: 'Please call me when you arrive at the train station',
    feedback: [
      {
        missing: 'ついたら',
        advice: 'The た-form of the verb arrive (つきます) is `ついたら`'
      }
    ]
  },
  {
    id: 'iWVwU46hS',
    ruleId: Keys.VERB_TA_RA_WHEN_AFTER_ONCE,
    hiragana: '60さい に だったら しごと を やめます',
    english: 'When I\'m 60, (I will) retire from work',
    feedback: [
      {
        missing: 'やめます',
        advice: 'The word for retire is `やめます`'
      }
    ]
  },
  {
    id: 'FmeryVZcy',
    ruleId: Keys.NOUN_DE_MO,
    hiragana: 'にちようび でも はたらきます',
    english: 'I will work, even if it is Sunday'
  },

] as ISentence[]