import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'vdSIY5RLx',
    ruleId: Keys.STATING_OPINION,
    hiragana: 'わたしは あした は あめが ふる と おもいます',
    english: 'I think it will rain tomorrow',
    feedback: [
      {
        missing: 'ふる',
        advice: 'The dictionary form of fall is ふる'
      }
    ]
  },
  {
    id: 'SB0IRglUB',
    ruleId: Keys.STATING_OPINION,
    hiragana: 'たぶん もう かえった と おもいます',
    english: 'I think he may have already gone home',
    feedback: [
      {
        missing: 'たべん',
        advice: 'Prefix with たぶん if the statement sounds uncertain (may have)'
      }
    ]
  }
] as ISentence[]