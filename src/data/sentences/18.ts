import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'A1WhijIFY',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: 'わたし は にほんご が できます',
    english: 'I can speak Japanese',
    feedback: [
      {
        match: 'にほんごを',
        advice: 'In the できます form, `か` preceeds できます, not `を`'
      }
    ]
  },
  {
    id: 'rPMzrjj_A',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: 'わたし は にほんご が できません',
    english: 'I can\'t speak japanese'
  },
  {
    id: 'zDLXizeWn',
    ruleId: Keys.DEKIMASU_VERB,
    hiragana: 'いくつ かんじ を かく こと が できあす か？',
    english: 'How many kanji can you write?',
    feedback: [
      {
        missing: 'かく',
        advice: 'The plan form of `write` (かきます) is `かく`'
      }
    ]
  },
  {
    id: 'E0dKnpXga',
    ruleId: Keys.DEKIMASU_VERB,
    hiragana: 'げんきん で はらう こと が できます か？',
    english: 'Can I pay with cash?',
    feedback: [
      {
        missing: 'げんきん',
        advice: 'The word for money/cash is `げんきん`'
      }
    ]
  },
  {
    id: 'VvwPuMTH-',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'しゅみ は なん です か',
    english: 'What are your hobbies?'
  },
  {
    id: 'MxY85pPvs',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は おんがく を きく こと です',
    english: 'My hobby is listening to music',
    feedback: [
      {
        missing: 'きく',
        advice: `The dictionary form of listen is 'きく'`
      }
    ]
  },
  {
    id: 'd0kIHMgQp',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は ほん を よむ こと です',
    english: 'My hobby is reading books',
    feedback: [
      {
        missing: 'よむ',
        advice: `The dictionary form of read is 'よむ'`
      }
    ]
  },
  {
    id: 'P10R4gCc0',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は くるま を なお する こと です',
    english: 'My hobby is reparing cars',
    feedback: [
      {
        missing: 'なお',
        advice: `The dictionary form of repair is 'なお すろ'`
      }
    ]
  },
  {
    id: '0jM9ZwrTM',
    ruleId: Keys.NAKANAKA,
    hiragana: 'にほん では なかなか うま を みる こと が できません',
    english: 'In Japan, you don\'t see many horses.',
  },
  {
    id: 'jOONlnuB9',
    ruleId: Keys.NAKANAKA,
    hiragana: 'わたし は かんじ を なかなか おぼえる こと が できません',
    english: 'I find it difficult to remember Kanjis',
    feedback: [
      {
        missing: 'なかなか',
        advice: 'The adverb for difficult is `なかなか`'
      }
    ]
  },
  {
    id: 'zwsxxpXIE',
    ruleId: Keys.ZEHI,
    hiragana: 'わたし は きもの お きた こと が ありません でも ぜひ きたい です',
    english: 'I have never worn a Kimono, but I really want to'
  },
] as ISentence[]