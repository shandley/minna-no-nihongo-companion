import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: '0zBWNMNpl',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: 'I ate lunch, then slept',
    hiragana: 'ひるごはん を たべて, ねました',
    feedback: [
      {
        missing: 'ひるごはん',
        advice: 'The word for lunch is `ひるごはん`'
      }
    ]
  },
  {
    id: 'nVk45hlVn',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: 'I will wake up at eight, have a bath, then go to school',
    hiragana: '8じに おきて、おふる を あびて, それからがこうに いきます',
    feedback: [
      {
        missing: 'おきて',
        advice: 'The て-form of wake up is `おきて`'
      },
      {
        missing: 'あびて',
        advice: 'The て-form of wash (bath/shower) is `あびて`'
      }
    ]
  },
  {
    id: 'uNLqfYPIr',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: 'I ate lunch, bought some bread, then went home',
    hiragana: 'ひるごはん を たべて, パン を かいて, うち へ かえりました',
    feedback: [
      {
        match: 'かいた',
        advice: 'Use the て form: かいて'
      },
      {
        match: 'たべた',
        advice: 'Use the て form: たべて'
      },
      {
        missing: 'ひるごはん',
        advice: 'The word for lunch is `ひるごはん`'
      }
    ]
  },
  {
    id: '6ACQAYwnN',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: `I went to school, studied kanji, then went home`,
    hiragana: `がっこう へ いって, かんじ を べんきょう して, うちへ かえりました`,
  },
  {
    id: '92dKq6HaD',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: `I went to the library, borrowed a book, then met my friend`,
    hiragana: `としょかん へ いって, ほん を かりて, ともだち に あいました`,
    feedback: [
      {
        match: 'ともだしに',
        advice: 'When meeting a friend, use the particle `に`'
      }
    ]
  },
  {
    id: 'Ln-K3uvGH',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: `Last night, I listened to a CD, read a book, then slept`,
    hiragana: `きのう の ばん CD を きいて, ほん を よんで, ねました`,
  },
  {
    id: 'Bo53NlCln',
    ruleId: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    english: 'This dog is small and cute',
    hiragana: 'この いぬ は ちさくて, かわいい です',
    feedback: [
      {
        match: 'ちさい',
        advice: 'Remember to remove the い (ちさくて)'
      }
    ]
  },
  {
    id: 'yjqL2rRWN',
    ruleId: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    english: 'This book is interesting and cheap',
    hiragana: 'この ほん は おもしろくて, やすい です',
  },
  {
    id: 'eoOT9l2na',
    ruleId: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    english: 'This apple is large and tasty',
    hiragana: 'この りんご は おおきくて おいしい です',
    feedback: [
      {
        missing: 'おおき',
        advice: 'The word for large is `おおき`. Remember the two お\'s'
      }
    ]
  },
  {
    id: 'gEXx_vRr2',
    ruleId: Keys.TE_FORM_NA_ADJ_JOIN_SENTENCES,
    english: 'Osaka is a quiet, beautiful city',
    hiragana: 'おおさか は しずか で, きれい な まち です',
  },
  {
    id: 'i8izwJyHP',
    ruleId: Keys.TE_FORM_NA_ADJ_JOIN_SENTENCES,
    hiragana: 'とうきょう は にぎやか で べんり な です',
    english: 'Tokyo is lively and convenient',
  },
  {
    id: 'J1AAnYP5R',
    ruleId: Keys.TE_FORM_KARA_THEN,
    hiragana: 'きのう しごと が おわって から いざかや に いきました',
    english: 'Yesterday, once work was finished, I went to the izakaya'
  },
  {
    id: 'yLey1qOdD',
    ruleId: Keys.NOUN_WO_VERB,
    hiragana: '8じに うち を でます',
    english: 'I leave home at 8'
  },
  {
    id: 'jZdo-jrTG',
    ruleId: Keys.NOUN_WO_VERB,
    hiragana: 'うえの で でんしゃ を おりました',
    english: 'I got off the train at Ueno',
    feedback: [
      {
        match: 'おります',
        advice: 'Pay attention the the sentence tense (おります > おりました)'
      }
    ]
  },
] as ISentence[]
