import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

const naiFormFeedback = [
  {
    match: 'ます',
    advice: 'Remember to use the ない verb form here'
  },
  {
    match: 'ないください',
    advice: '`で` should follow the verb to denote an action'
  }
]

const narimasenFeedback = [
  {
    missing: 'なりません',
    advice: 'In this form, なりません always exists'
  }
]

export default [
  {
    id: '2LdglIxYH',
    ruleId: Keys.NAI_FORM_PLEASE_DONT,
    english: 'Please don\'t take photos here',
    hiragana: 'ここ で しゃしん を とらない で ください',
    feedback: [
      {
        missing: 'とらない',
        advice: 'The dictionary form of といます is とら'
      }
    ]
  },
  {
    id: 'ESvM8WmzQ',
    ruleId: Keys.NAI_FORM_PLEASE_DONT,
    english: 'Please don\'t sit there',
    hiragana: 'そこ で すわない で ください',
    feedback: [
      ...naiFormFeedback,
      {
        missing: 'すわない',
        advice: 'The ない form of sit is すわない'
      }
    ]
  },
  {
    id: '71xNu8zIy',
    ruleId: Keys.NAI_FORM_PLEASE_DONT,
    english: 'Please don\'t sleep there',
    hiragana: 'そこ で ねない で ください',
    feedback: [
      ...naiFormFeedback,
      {
        missing: 'ねない',
        advice: 'The ない form of sleep is ねない'
      }
    ]
  },
  {
    id: 'H04oGoAGu',
    ruleId: Keys.NAI_FORM_PLEASE_DONT,
    hiragana: 'ここ に にもつ を おかない で ください',
    english: 'Please don\'t put your bags there'
  },
  {
    id: 'GcBDkvT-l',
    ruleId: Keys.NAI_FORM_PLEASE_DONT,
    english: 'Please don\'t smoke here',
    hiragana: 'ここ で すわない で ください',
    feedback: [
      ...naiFormFeedback,
      {
        missing: 'すわない',
        advice: 'The ない form of smoke is すわない'
      }
    ]
  },
  {
    id: 'OKk5oLHZe',
    ruleId: Keys.NAI_FORM_MUST_DO,
    hiragana: 'くすり を のま なければなりません',
    english: 'I have to take some medications',
    feedback: narimasenFeedback,
  },
  {
    id: 'B6ZMTe9eP',
    ruleId: Keys.NAI_FORM_MUST_DO,
    hiragana: 'あした びょういん へ いかなければなりません',
    english: 'I have to go to the hospital tomorrow',
    feedback: narimasenFeedback,
  },
  {
    id: 'ASCT2KAL2',
    ruleId: Keys.NAI_FORM_MUST_DO,
    hiragana: 'きにょうび まで に ほん を かえさなければなりません',
    english: 'I have to return this book by Friday',
    feedback: narimasenFeedback,
  },
  {
    id: 'yLeK4hprP',
    ruleId: Keys.NAI_FORM_MUST_DO,
    hiragana: 'もくようび まで に くるま を かえさなければなりません',
    english: 'I have to return this car by Thursday',
    feedback: narimasenFeedback,
  },
  {
    id: '1g6g-wp7t',
    ruleId: Keys.NAI_FORM_OPTIONAL,
    hiragana: 'あした こなくて も いいです',
    english: 'You don\'t have to come tomorrow'
  },
  {
    id: 'Xs1j00T5g',
    ruleId: Keys.NAI_FORM_OPTIONAL,
    hiragana: 'たべなくて も いいです',
    english: 'You don\'t have to eat'
  },
] as ISentence[]
