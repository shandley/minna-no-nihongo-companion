import { default as chapter01 } from './01'
import { default as chapter04 } from './04'
import { default as chapter05 } from './05'
import { default as chapter14 } from './14'
import { default as chapter15 } from './15'
import { default as chapter16 } from './16'
import { default as chapter17 } from './17'
import { default as chapter18 } from './18'
import { default as chapter19 } from './19'
import { default as chapter20 } from './20'
import { default as chapter21 } from './21'
import { default as chapter22 } from './22'
import { default as chapter23 } from './23'
import { default as chapter24 } from './24'
import { default as chapter25 } from './25'

export const sentences = [
  ...chapter01,
  ...chapter04,
  ...chapter05,
  ...chapter14,
  ...chapter15,
  ...chapter16,
  ...chapter17,
  ...chapter18,
  ...chapter19,
  ...chapter20,
  ...chapter21,
  ...chapter22,
  ...chapter23,
  ...chapter24,
  ...chapter25,
]