import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'dqTnfoC2e',
    ruleId: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    hiragana: 'いいえ、いちども ありません',
    english: 'No, I\'ve never',
  },
  {
    id: 'M-H9N2LqQ',
    ruleId: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    hiragana: 'うま に のった こと が あります か？',
    english: 'Have you ever ridden a horse?',
    feedback: [
      {
        missing: 'のった',
        advice: 'The た form of ride (のります) is のった'
      }
    ]
  },
  {
    id: 'kbZRMM9-3',
    ruleId: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    hiragana: 'おおさか へ いった こと が あります か',
    english: 'Have you ever been to Osaka',
    feedback: [
      {
        match: 'いって',
        advice: 'Use the た form (いった)'
      }
    ]
  },
  {
    id: '47iP5gffr',
    ruleId: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    hiragana: 'どこ で うま に のる こと が できます か',
    english: 'Where can you ride a horse?',
    feedback: [
      {
        match: 'を',
        advice: 'The correct particle here is `に`'
      },
      {
        missing: 'のる',
        advice: 'The dictionary form of `ride` is `のる`'
      }
    ]
  },
  {
    id: 'apgAztsX8',
    ruleId: Keys.BECOME_NA_ADJ_NI,
    hiragana: 'わたし は ゆうめい に なりたい です',
    english: 'I want to become famous',
    feedback: [
      {
        match: 'く',
        advice: 'ゆうめい is a な Adjective (use ゆうめい に...)'
      }
    ]
  },
  {
    id: 'gdj15bOQU',
    ruleId: Keys.BECOME_NA_ADJ_NI,
    hiragana: 'わたし の へや を きれい に なりました',
    english: 'I made my room very clean',
  },
  {
    id: 'qL7n300y8',
    ruleId: Keys.BECOME_NA_ADJ_NI,
    hiragana: 'びょうき でした が, くすり を のみました から, げんき に なりました',
    english: 'I felt Ill, I took some medicine, then I started feeling much better',
    feedback: [
      {
        missing: 'びょうき',
        advice: 'The word for sick/ill is `びょうき`'
      },
      {
        match: 'げんきく',
        advice: 'げんき is a な-adjective (us げんき に)'
      }
    ]
  },
  {
    id: 'nscbHxd_A',
    ruleId: Keys.BECOME_II_ADJ_KU,
    hiragana: 'せんせい は かんじ が たのし く なります',
    english: 'The teacher makes kanji more fun',
    feedback: [
      {
        match: 'たのしに',
        advice: 'たのしい is an い Adjective (use たのし く...)'
      }
    ]
  },
  {
    id: 'woZADAQw3',
    ruleId: Keys.BECOME_NOUN_NI,
    hiragana: 'わたし は ２９さい に なりました',
    english: 'I became 29 years old',
  },
] as ISentence[]
