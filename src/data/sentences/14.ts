import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: '-MXusJdtK',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが この かんじ の よみかた を おしえて ください',
    english: 'Excuse me, could you tell me how to read this kanji please?',
    feedback: [
      {
        missing: 'よみかた',
        advice: 'The method of reading is よみかた'
      },
      {
        match: 'かんじが',
        advice: 'This should be かんじ `の` ...'
      }
    ]
  },
  {
    id: 'Co9nj9ObN',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが しお を とって ください',
    english: 'Excuse me, could you please pass the salt?',
    feedback: [
      {
        missing: 'しお',
        advice: 'The word for salt is `しお`'
      }
    ]
  },
  {
    id: 'x1xmw2bEU',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが しゃしん を とって ください',
    english: 'Excuse me, could take a picture?'
  },
  {
    id: '8YSmEB5l9',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが もう すこし ゆっくり はなして ください',
    english: 'Excuse me, could you please speak more slowly?',
    feedback: [
      {
        match: 'ゆくり',
        advice: '(Spelling) ゆくり -> ゆっくり'
      },
      {
        match: 'をはなして',
        advice: 'The particle `を` should not be here: `slowly` is not an object.'
      }
    ]
  },
  {
    id: 'L5s1wYZiB',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'もちろん, たべて ください',
    english: 'Of course, Please eat.',
    feedback: [
      {
        missing: 'もちろん',
        advice: '`Of course` is expressed with `もちろん`'
      }
    ]
  },
  {
    id: 'llM08R-xt',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'どうぞ のんで ください',
    english: 'Go ahead, Please drink.'
  },
  {
    id: 'N-vBjN-tw',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'すわて ください',
    english: 'Please sit',
    feedback: [
      {
        missing: 'すわて',
        advice: 'The て-form of sit is `すわて`'
      }
    ]
  },
  {
    id: 'bSei7fMbZ',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'はいって ください',
    english: 'Please come in (enter)',
    feedback: [
      {
        missing: 'はいって',
        advice: 'The て-form of enter is `はいって`'
      }
    ]
  },
  {
    id: 'AU_2O0Gp6',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'なまえ を かいて ください',
    english: 'Please write your name',
    feedback: [
      {
        missing: 'かいて',
        advice: 'The て-form of write is `かいて`'
      }
    ]
  },
  {
    id: 'PnVcNkdXB',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'しょくだい を だして ください',
    english: 'Please hand in your homework',
    feedback: [
      {
        missing: 'しょくだい',
        advice: 'The word for homework is `しょくだい`'
      },
      {
        missing: 'だして',
        advice: 'The て-form of `hand in` is `だして`'
      }
    ]
  },
  {
    id: 'EUqVsMV7h',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: `ちず を みせて ください`,
    english: 'Please show the map',
    feedback: [
      {
        missing: 'みせて',
        advice: 'The て-form of show is `みせて`'
      }
    ]
  },
  {
    id: 'MMnh1biPJ',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'にもつ を おけて ください',
    english: 'Please open your luggage',
    feedback: [
      {
        missing: 'おけて',
        advice: 'The て-form of `open` is `おけて`'
      }
    ]
  },
  {
    id: 'u2kYnDYqd',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'まど を しめて ください',
    english: 'Please close the window',
    feedback: [
      {
        missing: 'しめて',
        advice: 'The て-form of `close` is `しめて`'
      }
    ]
  },
  {
    id: 'M9jiA4GGx',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'おんがく を けして ください',
    english: 'Please turn off that music',
    feedback: [
      {
        missing: 'けして',
        advice: 'The て-form of `turn off` is `けして`'
      }
    ]
  },
  {
    id: 'jzjmLR4mX',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'じゅうしょ を かいて ください',
    english: 'Please write your address',
    feedback: [
      {
        missing: 'かいて',
        advice: 'The て-form of write is `かいて`'
      },
      {
        missing: 'じゅうしょ',
        advice: 'The word for address is じゅうしょ'
      }
    ]
  },
  {
    id: 'GoH-Mvkuf',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'いま あめ が ふって います か',
    english: 'Is it raining right now?',
    feedback: [
      {
        missing: 'ふって',
        advice: 'The て-form of ふります is ふって'
      },
      {
        match: 'あります',
        advice: 'An event that is in progress should be denoted by います'
      }
    ]
  },
  {
    id: '9Xm8iJIml',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'はい、いま ゆき が ふって います',
    english: 'Yes, it\'s snowing right now',
    feedback: [
      {
        match: 'あります',
        advice: 'An event that is in progress should be denoted by います'
      }
    ]
  },
  {
    id: 'bvnPATyvZ',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'きのう わたしは ほんや の まえ に ２０ぷん かのじょ を まちました',
    english: 'Yesterday I waited in front of the bookshop for 20 minutes for my girlfriend',
    feedback: [
      {
        missing: 'まちました',
        advice: 'The past tense of wait is `まちました`'
      }
    ]
  },
  {
    id: 'RXau_Xk1u',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'なに を して います か',
    english: 'What are you doing?',
  },
  {
    id: 'lQuN-VqQK',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'でんわ を はなして います',
    english: '(I\'m) talking on the phone',
  },
  {
    id: 'v-H2aSp_A',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'でんしゃ を まって います',
    english: '(I\'m) waiting for the train',
  },
  {
    id: 'yLUnUUglN',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'にもつ を もちましょうか',
    english: 'Can I carry your luggage?',
    feedback: [
      {
        match: 'が',
        advice: 'Use を as this denotes an action involving a subject (にもつ)'
      },
      {
        missing: 'もちましょうか',
        advice: 'The Presumptive form of もちます is もちましょう'
      }
    ]
  },
  {
    id: 'FwsDCtoHk',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'あつい です ね? まど を あけましょうか?',
    english: 'It\'s hot huh? Should I open the window?',
    feedback: [
      {
        missing: 'まど',
        advice: 'The word for window is まど'
      },
      {
        missing: 'あけましょうか',
        advice: 'The Presumptive form of あけます is あけましょう'
      }
    ]
  },
  {
    id: 'RSq5UXEX8',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'さむい です ね? まど を しめましょう か?',
    english: 'It\'s cold huh? Should I close the window?',
    feedback: [
      {
        missing: 'まど',
        advice: 'The word for window is まど'
      },
      {
        missing: 'しめましょう',
        advice: 'The Presumptive form of しめます is しめましょう'
      }
    ]
  },
  {
    id: 'eVhiQmXJE',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'くらい です ね? でんき を つけてましょうか?',
    english: 'It\'s dark in here huh? Should I turn on the light?',
    feedback: [
      {
        missing: 'でんき',
        advice: 'The word for light is でんき'
      },
      {
        missing: 'つけてましょうか',
        advice: 'The Presumptive form of `turn on` is つけてましょうか'
      }
    ]
  },
  {
    id: 'CJf4q0sKv',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'えき まで むかえ に いきましょうか？',
    english: 'Should I greet you at the train station?',
    feedback: [
      {
        missing: 'むかえ',
        advice: 'The plain form of むかえます is むかえ'
      }
    ]
  },
] as ISentence[]
