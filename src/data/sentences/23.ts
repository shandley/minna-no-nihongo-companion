import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'f6wUVYUCy',
    ruleId: Keys.MISC,
    hiragana: 'せんせい に ききます',
    english: 'Ask the teacher'
  },
  {
    id: 'E4DVDCQ-Y',
    ruleId: Keys.TOKI_II_ADJ,
    hiragana: 'あたま が いたい とき この くすり を のみます',
    english: 'When I have a headache, I take medicine'
  },
  {
    id: 'BikGnptY3',
    ruleId: Keys.TOKI_II_ADJ,
    hiragana: 'わかい とき あまり べんきょう しません でした',
    english: 'When I have a headache, I take medicine'
  },
  {
    id: 'YYZbDqKll',
    ruleId: Keys.TOKI_NOUN,
    hiragana: 'こども の とき よく かわ で およぎまあした',
    english: 'I often swam in the river when I was a child'
  },
  {
    id: '1bI47uroJ',
    ruleId: Keys.TOKI_NA_ADJ,
    hiragana: 'ひま な とき うちへ あそび に きませんか',
    english: 'If you have some free time, come visit me'
  },
  {
    id: 'hwXFYNNFX',
    ruleId: Keys.NOUN_WO_MOTION_VERB,
    hiragana: 'この こうさてん を みぎ へ まがります',
    english: 'Turn right at that intersection'
  },
  {
    id: 'fBKdTzKoM',
    ruleId: Keys.NOUN_WO_MOTION_VERB,
    hiragana: 'この はし を わたって, ひだり へ まがります',
    english: 'Cross over that bridge, then turn left'
  },
] as ISentence[]