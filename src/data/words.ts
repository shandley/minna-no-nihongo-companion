import {
  IVocabularyItem,
  IVerbOption,
  INoun,
  Noun
} from '../interfaces'

export const verbOptions: IVerbOption[] = [
  {
    verbId: 'rlE_1a8HG3',
    availableNouns: [
      Noun.TOBACCO
    ]
  },
  {
    verbId: '2gtqxhT4fHb',
    availableNouns: [
      Noun.PHOTO
    ]
  },
  {
    verbId: 'oT9HKYi2W',
    availableNouns: []
  },
  {
    verbId: 'M8p1zBrSjX',
    availableNouns: []
  },
  {
    verbId: 'itaJkn2L_',
    availableNouns: [
      Noun.RAMEN,
      Noun.YAKISOBA
    ]
  }
]

export const nouns: INoun[] = [
  {
    key: Noun.TOBACCO,
    chapter: 0,
    hiragana: 'たばこ',
    english: 'tobacco'
  },
  {
    key: Noun.BOOK,
    chapter: 2,
    hiragana: 'ほん',
    english: 'book'
  },
  {
    key: Noun.DICTIONARY,
    chapter: 2,
    hiragana: 'じしょ',
    english: 'dictionary'
  },
  {
    key: Noun.ALCAHOL,
    chapter: 0,
    hiragana: 'おさけ',
    english: 'alcahol'
  },
  {
    key: Noun.PHOTO,
    chapter: 0,
    hiragana: 'しゃしん',
    english: 'photos',
    type: 'plural'
  },
  {
    key: Noun.PASSPORT,
    chapter: 0,
    hiragana: 'パスポート',
    english: 'passport'
  },
  {
    key: Noun.RAMEN,
    chapter: 0,
    hiragana: 'ラメン',
    english: 'ramen'
  },
  {
    key: Noun.YAKISOBA,
    chapter: 0,
    hiragana: 'やきそば',
    english: 'yakisoba'
  }
]

export const locations: IVocabularyItem[] = [
  {
    hiragana: 'ここ',
    english: 'here'
  },
  {
    hiragana: 'そこ',
    english: 'there'
  },
  {
    hiragana: 'あそこ',
    english: 'over there'
  }
]

export const places: IVocabularyItem[] = [
  {
    hiragana: 'ぎんこう',
    english: 'the bank'
  },
  {
    hiragana: 'びょいん',
    english: 'the hospital'
  },
  {
    hiragana: 'こえん',
    english: 'the park'
  }
]

export const cities: IVocabularyItem[] = [
  {
    hiragana: 'とうきょう',
    english: 'Tokyo'
  },
  {
    hiragana: 'おおさか',
    english: 'Osaka'
  },
  {
    hiragana: 'うえの',
    english: 'Ueno'
  },
  {
    hiragana: 'しぶや',
    english: 'Shibuya'
  }
]