import { hiragana as hiraganaData, kanji as kanjiData } from '../data'

const getHiraganaMatch = (input: string) =>
  hiraganaData.find(h => h.hiragana === input)

const getKanjiMatch = (input: string) =>
  kanjiData.find(k => k.kanji === input)

const isKanji = (input: string) =>
  [...input].filter(
    (char: string) => /^[\u4e00-\u9faf]+$/.test(char)
  ).length === input.length

const hasKanji = (input: string) =>
  [...input].filter(
    (char: string) => /^[\u4e00-\u9faf]+$/.test(char)
  ).length

const hasHiragana = (input: string) =>
  [...input].filter(
    (char: string) => /^[\u3040-\u309F]+$/.test(char)
  ).length

const rubyiseKanji = (input: string) => {
  const match = getKanjiMatch(input)

  if (!match) {
    return input
  }

  return match.ruby.map(r =>
    `<ruby>${r.kanji}<rp>(</rp><rt>${r.hiragana}</rt><rp>)</rp></ruby>`
  ).join('')
}

export const withRuby = (input: string): string => {
  if (isKanji(input)) {
    return rubyiseKanji(input)
  }

  if (hasKanji(input) && hasHiragana(input)) {
    return [...input].map((c: string) => isKanji(c) ? rubyiseKanji(c) : c).join('')
  }

  return input.split(' ').map((h => {
    const match = getHiraganaMatch(h)

    return match
      ? `<ruby>${match.kanji}<rp>(</rp><rt>${match.replace}</rt><rp>)</rp></ruby>${match.add}`
      : h
  })).join(' ')
}

// Need to be careful of > 1 chaacter in succession to wrap it in the same ruby
// <ruby>東<rp>(</rp><rt>とう</rt><rp>)</rp>京<rp>(</rp><rt>きょう</rt><rp>)</rp></ruby>
