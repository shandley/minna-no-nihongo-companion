import verbs, { Verb } from 'minna-no-nihongo-verbs'
import {
  randomArrayItem,
  stripWhitespace,
  filterByChapterRange,
  stripPunctuation,
  replaceNumbers
} from '../utils'

import {
  cities,
  locations,
  places,
  nouns,
  verbOptions,
  sentences
} from '../data'

import {
  INoun,
  IQuestionCollection,
  IQuestionResultFeedback,
  ISentenceTesterOptions,
  ISentenceTesterQuestion,
  IVerbOption,
  IVocabularyItem,
} from '../interfaces'

import { getRuleById } from './'
import { Keys } from '../data/grammar/keys'

export interface ISentence extends IVocabularyItem {
  readonly id: string
  readonly ruleId: Keys
  feedback?: IQuestionResultFeedback[]
  instructions?: string
}

const getVerb = (id: string) => verbs.find(verb => verb.id === id)

const getNaiForm = (verbItem: string): string => (
  verbs.find(v => v.polite_form.present === verbItem) as Verb).plain_form.present_negative

type variables = 'noun' | 'verb' | 'location' | 'nounNaiForm' | 'place' | 'city'

const contains = (sentence: ISentence): { [key in variables]: () => boolean } => ({
  noun: () => [sentence.hiragana, sentence.hiragana].join('').indexOf('{noun') > -1,
  nounNaiForm: () => [sentence.hiragana, sentence.hiragana].join('').indexOf('{noun.NaiForm') > -1,
  verb: () => [sentence.hiragana, sentence.hiragana].join('').indexOf('{verb') > -1,
  location: () => [sentence.hiragana, sentence.hiragana].join('').indexOf('{location') > -1,
  place: () => [sentence.hiragana, sentence.hiragana].join('').indexOf('{place') > -1,
  city: () => [sentence.hiragana, sentence.hiragana].join('').indexOf('{city') > -1,
})

type MaybeUndefined<T> = T | undefined

export const getSentenceQuestionItems = (
  options: ISentenceTesterOptions
): IQuestionCollection<ISentenceTesterQuestion> => {
  const result: IQuestionCollection<ISentenceTesterQuestion> = {
    questions: []
  }

  const availableSentences = filterByChapterRange(sentences, options)

  if (!availableSentences.length) {
    return {
      ...result,
      message: 'No items were found which match the filters provided'
    }
  }

  const nums = new Set<number>()

  while (nums.size !== Math.min(options.NUM_QUESTIONS, availableSentences.length)) {
    nums.add(Math.floor(Math.random() * availableSentences.length - 1) + 1)
  }

  return {
    ...result,
    questions: [...nums].map((num: number) =>
      getSentenceQuestionItem(num, availableSentences)
    )
  }
}

export const getSentenceQuestionItem = (
  index: number,
  availableSentences: ISentence[]
): ISentenceTesterQuestion => {

  const sentence: ISentence = availableSentences[index]

  let location: MaybeUndefined<IVocabularyItem>
  let city: MaybeUndefined<IVocabularyItem>
  let place: MaybeUndefined<IVocabularyItem>
  let verb: MaybeUndefined<Verb>
  let noun: MaybeUndefined<INoun>

  if (contains(sentence).location()) {
    location = randomArrayItem(locations) as IVocabularyItem
  }

  if (contains(sentence).city()) {
    city = randomArrayItem(cities)
  }

  if (contains(sentence).place()) {
    place = randomArrayItem(places) as IVocabularyItem
  }

  if (contains(sentence).verb()) {
    const verbItem = randomArrayItem(verbOptions) as IVerbOption
    verb = getVerb(verbItem.verbId)

    // Not all items include a noun
    if (verbItem.availableNouns && verbItem.availableNouns.length) {
      const nounKey = randomArrayItem(verbItem.availableNouns)
      noun = nouns.find(n => n.key === nounKey) as INoun
    }
  } else {
    if (contains(sentence).noun()) {
      noun = randomArrayItem(nouns) as INoun
    }
  }

  const rule = getRuleById(sentence.ruleId)

  const hiragana = sentence.hiragana
    .replace('{verb}', verb ? verb.polite_form.present : '')
    .replace('{verb.NaiForm}', verb ? getNaiForm(verb.polite_form.present) : '')
    .replace('{noun}', noun ? noun.hiragana : '')
    .replace('{location}', location ? location.hiragana : '')
    .replace('{place}', place ? place.hiragana : '')
    .replace('{city}', city ? city.hiragana : '')

  const english = sentence.english
    .replace('{verb}', verb ? verb.english : '')
    .replace('{noun}', noun ? noun.english : '')
    .replace('{location}', location ? location.english : '')
    .replace('{place}', place ? place.english : '')
    .replace('{city}', city ? city.english : '')

  const instructions = sentence.instructions || ''

  return {
    id: sentence.id,
    answer: [hiragana],
    question: english,
    options: [''],
    meta: {
      rule,
      sentence,
      instructions
    }
  }
}

// Todo, use a proper chaining method, a la ramda
const strip = (input: string) => 
  stripPunctuation(
    stripWhitespace(
      replaceNumbers(input)
    )
  )

const equalStrings = (a: string, b: string) => strip(a) === strip(b)

export const compareSentences = (
  submittedSentence: string,
  sentenceReference: string
): {
  correct: boolean
  feedback?: IQuestionResultFeedback
} => {
  const ignoredPrefixes = ['わたしは']
  const ignoredSuffixes = ['です']

  const directMatch = () => equalStrings(submittedSentence, sentenceReference)
  const prefixMatch = () => ignoredPrefixes.filter(prefix =>
    equalStrings(prefix + submittedSentence, sentenceReference)
  ).length > 0
  const suffixMatch = () => ignoredSuffixes.filter(suffix =>
    equalStrings(submittedSentence + suffix, sentenceReference)
  ).length > 0

  return {
    correct: directMatch() || prefixMatch() || suffixMatch()
  }
}

export const getSentenceQuestionFeedback = (
  question: ISentenceTesterQuestion,
  submittedSentence: string,
): IQuestionResultFeedback | null => {
  const { feedback } = question.meta.sentence

  if (!feedback) {
    return null
  }

  const userSubmission = strip(submittedSentence)

  const result = feedback.map((feedbackItem) => {
    if ('match' in feedbackItem) {
      return (userSubmission.includes((feedbackItem.match as string)))
        ? feedbackItem as IQuestionResultFeedback
        : null
    } else if ('missing' in feedbackItem) {
      return !userSubmission.includes((feedbackItem.missing as string))
        ? feedbackItem as IQuestionResultFeedback
        : null
    }

    return null
  }).filter(i => i !== null)

  return result.length
    ? result[0]
    : null
}

export const matchSpacing = (sentence: string, reference: string): string => {
  if (sentence === reference) {
    return sentence
  }

  if (strip(sentence) === strip(reference)) {
    return spaceMatchingSentence(sentence, reference)
  }

  return sentence
}

export const getSentenceById = (sentenceId: string): ISentence =>
  sentences.find((s: ISentence) => s.id === sentenceId) as ISentence

const spaceMatchingSentence = (sentence: string, reference: string) => {
  const words: string[] = []

  reference.split(' ').map((word: string) => {
    if (sentence.indexOf(word) > -1) {
      const index = reference.indexOf(word)
      words.push(reference.slice(index, index + word.length))
    }
  })

  return words.join(' ')
}

// Would be nice to get this

// これは おおさか で かいた え です
// おさかでかいたえです

// {
//  missing: 'これは',
//  spelling: 'おおさか',
//  missing: 'で'
//  correct: 'かいた え です'
// }