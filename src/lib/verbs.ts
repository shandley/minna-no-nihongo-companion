import verbs, { Verb } from 'minna-no-nihongo-verbs'
import { shuffle } from '../utils'
import {
  IVerbConjugationTesterOptions,
  IVerbConjugationTesterQuestion,
  VerbType,
  VerbTypeLabels
} from '../interfaces'

export const getVerbById = (id: string): Verb => (verbs.find(v => v.id === id)) as Verb

const getRandomNumberSet = (options: IVerbConjugationTesterOptions) => {
  const nums = new Set<number>()

  while (nums.size !== options.NUM_QUESTIONS) {
    nums.add(Math.floor(Math.random() * verbs.length - 1) + 1)
  }

  return [...nums]
}

const getMeta = (item: Verb) => ({ english: item.english, kanji: item.kanji })

export const getVerbConjugationQuestions = (
  options: IVerbConjugationTesterOptions
): IVerbConjugationTesterQuestion[] => {
  return getRandomNumberSet(options).map((num: number) =>
    getVerbConjugationQuestionItem(num, options)
  )
}

const getVerbConjugationQuestionItem = (
  index: number,
  formats: IVerbConjugationTesterOptions
): IVerbConjugationTesterQuestion => {
  const { QUESTION_VERB_FORM = VerbType.POLITE, ANSWER_VERB_FORM = VerbType.PLAIN } = formats

  const itemData = verbs[index]
  const item = {
    ...itemData,
    nai_form: {
      present: itemData.plain_form.present_negative
    },
    ta_form: {
      present: itemData.plain_form.past
    }
  }

  const answer: string = item[ANSWER_VERB_FORM].present

  const options = shuffle([
    ...getVerbDecoys(item[ANSWER_VERB_FORM].present, ANSWER_VERB_FORM).filter((i) => i !== answer).slice(0, 3),
    answer
  ])

  const meta = {
    ...getMeta(item),
    instructions: `Convert from ${VerbTypeLabels[QUESTION_VERB_FORM]} to ${VerbTypeLabels[ANSWER_VERB_FORM]}`
  }

  return {
    id: item.id,
    question: item[QUESTION_VERB_FORM].present,
    answer: [answer],
    options,
    meta
  }
}

const getVerbDecoys = (input: string, type: VerbType): string[] => {
  switch (type) {
    case VerbType.PLAIN:
      return getVerbPlainFormDecoys(input)
    case VerbType.NAI:
      return getVerbNegationDecoys(input)
    case VerbType.POLITE:
      return getPoliteFormDecoys(input)
    case VerbType.TA:
      return getTaFormDecoys(input)
    case VerbType.TE:
      return getTeFormDecoys(input)
  }
}

const getPoliteFormDecoys = (input: string): string[] => {
  return [
    input.replace(input.slice(input.length - 2), 'ます'),
    input.replace(input.slice(input.length - 3), 'します'),
    input.replace(input.slice(input.length - 3), 'います'),
    input.replace(input.slice(input.length - 3), 'ました')
  ]
}

const getVerbNegationDecoys = (input: string): string[] => {
  return [
    input.replace(input.slice(input.length - 3), 'ない'),
    input.replace(input.slice(input.length - 3), 'わないい'),
    input.replace(input.slice(input.length - 3), 'さんい'),
    input.replace(input.slice(input.length - 3), 'くない'),
    input.replace(input.slice(input.length - 3), 'じゃない')
  ]
}

const getTaFormDecoys = (input: string): string[] =>
  getTeFormDecoys(input).map(
    (i: string) => i.replace('て', 'た').replace('で', 'だ'))

const getTeFormDecoys = (input: string): string[] => {
  return [
    input.replace(input.slice(input.length - 2), 'って'),
    input.replace(input.slice(input.length - 2), 'っで'),
    input.replace(input.slice(input.length - 2), 'んで'),
    input.replace(input.slice(input.length - 1), 'て'),
    input.replace(input.slice(input.length - 1), 'いて'),
  ]
}

const getVerbPlainFormDecoys = (input: string): string[] => {
  return [
    input.replace(input.slice(input.length - 1), 'く'),
    input.replace(input.slice(input.length - 1), 'る'),
    input.replace(input.slice(input.length - 1), 'わ'),
    input.replace(input.slice(input.length - 1), 'す'),
    input.replace(input.slice(input.length - 1), 'ぬ')
  ]
}
