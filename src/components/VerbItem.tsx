import * as React from 'react'
import { Verb } from 'minna-no-nihongo-verbs'
import Panel, { PanelHeading, PanelDivider, PanelActions } from './Panel'
import Button from './Button'

const VerbItem: React.FC<{
  verb: Verb,
  destroy: () => void
}> = ({ verb, destroy }) => {
  const { polite_form, plain_form, te_form } = verb

  return (
    <Panel>
      <PanelHeading
        heading={polite_form.present + ' / ' + verb.english}
        subheading={`Group ${verb.verb_group} verb`}
      />
      <PanelDivider />
      <p><strong>Kanji</strong>: {verb.kanji}</p>
      <p><strong>Plain. Form</strong>: {plain_form.present}</p>
      <p><strong>Plain. Form (Past)</strong>: {plain_form.past}</p>
      <p><strong>Negative-Form</strong>: {plain_form.present_negative}</p>
      <p><strong>Negative-Form (Past)</strong>: {plain_form.past_negative}</p>
      <p><strong>て-Form</strong>: {te_form.present}</p>
      <p><strong>Chapter</strong>: {verb.chapter}</p>
      <PanelDivider />
      <PanelActions>
        <Button
          label="Close"
          action={() => destroy()}
        />
      </PanelActions>
    </Panel>
  )
}

export default VerbItem