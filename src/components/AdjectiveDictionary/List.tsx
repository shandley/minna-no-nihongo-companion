import * as React from 'react'
import styled from '@emotion/styled'
import { Adjective } from 'minna-no-nihongo-adjectives'
import AdjectiveItem from '../AdjectiveItem'
import { MODAL } from '../Modal'
import { useDispatch } from 'react-redux'
import { show, destroy } from 'redux-modal'
import BaseTable from '../Base/Table'
import { theme } from '../../data'

enum AdjectiveType {
  'い',
  'な'
}

const Table = styled(BaseTable)`
  th {
    padding: 0.5rem;
    background-color: ${theme.colours.grey.dark}
  }

  td:nth-of-type(2) {
    width: 1rem;
    text-align: center;
  }
`

const AdjectiveList: React.FC<{ adjectives: Adjective[] }> = ({ adjectives }) => {
  const dispatch = useDispatch()

  const showModal = (a: Adjective) => {
    dispatch(show(MODAL, {
      content: <AdjectiveItem
        adjective={a}
        destroy={() => dispatch(destroy(MODAL))}
      />
    }))
  }

  return (
    <Table>
      <thead>
        <tr>
          <th>Hiragana</th>
          <th>Chapter</th>
          <th>English</th>
        </tr>
      </thead>
      <tbody>
        {adjectives.map((a) => (
          <tr key={a.id} onClick={() => showModal(a)}>
            <td>{a.hiragana} ({AdjectiveType[a.adjective_type]})</td>
            <td>{a.chapter}</td>
            <td>{a.english}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  )
}

export default AdjectiveList
