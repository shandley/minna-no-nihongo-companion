import * as React from 'react'
import { PanelActions } from '../../Panel'
import Button from '../../Button'
import { useDispatch } from 'react-redux'
import { quitActivity, skipQuestion } from '../../../store/actions/activity'

const Actions: React.FC<{}> = () => {
  const dispatch = useDispatch()

  return (
    <PanelActions>
      <Button label='Quit' action={() => dispatch(quitActivity())} />
      <Button label='Skip Question' action={() => dispatch(skipQuestion())} />
    </PanelActions>
  )
}

export default Actions
