import * as React from 'react'
import { PanelDivider, PanelHeading } from '../../../Panel'
import HighlightedText from '../../../HighlightedText'
import { IQuestionResultFeedback } from '../../../../interfaces'
import Text from '../../../Text'

const Feedback: React.FC<{
  feedback: IQuestionResultFeedback | null | undefined,
  submittedAnswer: string[]
}> = ({ feedback, submittedAnswer }) => {
  if (!feedback) {
    return null
  }

  const { missing, match, advice } = feedback
  let highlightItem = null

  if (match) {
    highlightItem = <HighlightedText text={submittedAnswer.join('')} highlight={match} />
  } else if (missing) {
    highlightItem = <HighlightedText text={`Absent: ${missing}`} highlight={missing} />
  }

  return (
    <React.Fragment>
      <PanelDivider />
      <PanelHeading
        heading={'Feedback'}
      />
      {highlightItem}
      <Text>{advice}</Text>
    </React.Fragment>
  )
}

export default Feedback