import * as React from 'react'
import { formatDateShort, collateFeedback } from '../../../lib'
import { IDBResultItem, IDBResult } from '../../../interfaces'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../../Panel'
import Table from '../Table'
import Button from '../../Button'
import Feedback from './Results/Feedback'

const QuestionHistory: React.FC<{ result: IDBResult, destroy: () => void }> = ({ result, destroy }) => {
  const feedback = collateFeedback(result)

  const { results } = result

  return (
    <Panel>
      <PanelHeading
        heading="Question history"
        subheading={`You've previously answered this question incorrectly:`}
      />
      {feedback.length
        ? feedback.map(f =>
          <Feedback
            key={f.result.time}
            feedback={f.feedback}
            submittedAnswer={f.result.submittedAnswer as string[]}
          />
        )
        : <PanelDivider />
      }
      <Table>
        <tbody>
          {results.filter(item => !item.correct)
            .sort()
            .map((item: IDBResultItem) => (
              <tr key={item.time}>
                <td>{formatDateShort(new Date(item.time))}</td>
                <td>{item.submittedAnswer}</td>
              </tr>
            ))}
        </tbody>
      </Table>
      <PanelDivider />
      <PanelActions>
        <Button
          label="Close"
          action={() => destroy()}
        />
      </PanelActions>
    </Panel>
  )
}

export default QuestionHistory