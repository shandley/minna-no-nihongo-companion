import * as React from 'react'
import styled from '@emotion/styled'
import { useDispatch } from 'react-redux'
import { setOption } from '../../../store/actions/activity'
import { getOptionsForActivity } from '../../../lib'
import { IOptionState } from '../../../store/reducers/activity'
import Input from '../../Input'
import Select from '../../Select'
import { theme } from '../../../data'
import { IActivityOption, OptionType } from '../../../interfaces'

const OptionsItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin: 0.2rem 0;
  align-items: center;

  label {
    font-family: ${theme.fontFamily};
    font-weight: lighter;
    font-size: ${theme.fontSize.slightlySmaller};
    padding: 0.1rem;
    width: 100%;
  }
`

const StyledInput = styled(Input)`
  max-width: 3rem;
`

const StyledSelect = styled(Select)`
  max-width: 8rem;
`

type MinOrMaxValueConstraint = {
  [key in 'min' | 'max']: number
}

const getDependencyConstraints = (option: IActivityOption, values: any) => {
  const { dependency } = option

  if (!dependency || !values || !values.hasOwnProperty(dependency.option)) {
    return null
  }

  const { option: dependentOption, rule } = dependency
  const dependencyValue = values[dependentOption]

  if (option.type === OptionType.NUMBER) {
    return {
      [rule]: rule === 'max' ? dependencyValue - 1 : dependencyValue + 1
    }
  }

  return {
    [dependencyValue]: true
  }
}

const Option: React.FC<{
  activityId: number,
  option: IActivityOption,
  options: IOptionState
}> = ({ activityId, option, options }) => {
  const dispatch = useDispatch()
  const { title, id, type, values: selectOptions } = option

  const values = options
    ? getOptionsForActivity(options, activityId)
    : {}

  const constraints = getDependencyConstraints(option, values) || {}

  const value = values && values.hasOwnProperty(id) ? values[id] : option.default

  const numericOptionUpdate = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const { valid } = event.target.validity

    if (!valid) {
      return
    }

    const { id: optionKey, value: optionValue } = event.target

    dispatch(setOption({
      activityId,
      optionKey,
      optionValue: parseInt(optionValue, 10)
    }))
  }

  const stringOptionUpdate = (
    event: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    const { valid } = event.target.validity

    if (!valid) {
      return
    }

    const { id: optionKey, value: optionValue } = event.target

    dispatch(setOption({
      activityId,
      optionKey,
      optionValue: optionValue.toString()
    }))
  }

  switch (type) {
    case OptionType.STRING:
      return (
        <OptionsItemContainer>
          <label htmlFor={id}>{title}</label>
          <StyledSelect
            id={id}
            value={value ? value : ''}
            onChange={stringOptionUpdate}
          >
            {
              selectOptions!.map((optionItem) => {
                return (
                  <option
                    disabled={constraints.hasOwnProperty(optionItem.value)}
                    key={optionItem.value}
                    id={optionItem.value}
                    value={optionItem.value}
                  >
                    {optionItem.label}
                  </option>
                )
              })
            }
          </StyledSelect>
        </OptionsItemContainer>
      )
    case OptionType.NUMBER:
    default:
      return (
        <OptionsItemContainer>
          <label htmlFor={id}>{title}</label>
          <StyledInput
            {...constraints as MinOrMaxValueConstraint}
            id={id}
            value={value ? value : ''}
            type="number"
            onChange={numericOptionUpdate}
          />
        </OptionsItemContainer>
      )
  }
}

export default Option