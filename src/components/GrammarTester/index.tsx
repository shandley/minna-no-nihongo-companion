import * as React from 'react'
import { useState } from '../../hooks'
import Options from '../Base/Quiz/Options'
import Quiz from './Quiz'
import Intro from '../Base/Quiz/Intro'
import Results from '../Base/Quiz/Results'

const GrammarTester: React.FC<{}> = () => {
  const activity = useState(state => state.activity)

  return activity.running
    ? <Quiz activity={activity} />
    : activity.complete
      ? <Results activity={activity} />
      : (
        <React.Fragment>
          <Intro activity={activity} />
          <Options activity={activity} />
        </React.Fragment>
      )
}

export default GrammarTester
