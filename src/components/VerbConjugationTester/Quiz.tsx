import * as React from 'react'
import styled from '@emotion/styled'
import { useDispatch } from 'react-redux'
import { IVerbConjugationTesterQuestion } from '../../interfaces'
import { theme } from '../../data'
import Actions from '../Base/Quiz/Actions'
import { submitAnswer } from '../../store/actions/activity'
import { IActivityState } from '../../store/reducers/activity'
import Button from '../Button'
import Text from '../Text'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../Panel'

const OptionsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 0.7rem;

  button + button {
    margin-left: 0;
  }
`

const Instructions = styled(Text)`
  margin-top: 0;
`

const Quiz: React.FC<{
  activity: IActivityState
}> = ({ activity }) => {
  const dispatch = useDispatch()

  const { correct, currentQuestion } = activity
  const { question, options, questionNumber, meta } = currentQuestion as IVerbConjugationTesterQuestion

  const { english, kanji, instructions } = meta

  const { green, red } = theme.colours

  const borderTop = typeof correct === "boolean"
    ? correct
      ? green.dark
      : red.dark
    : 'transparent'

  return (
    <Panel borderTop={borderTop}>
      {instructions ? <Instructions>{instructions}:</Instructions> : null}
      <PanelHeading
        heading={`Q.${questionNumber}: ${question}`}
        subheading={kanji + ': ' + english}
      />
      <PanelDivider />
      <PanelActions>
        <OptionsContainer>
          {options.map((option: string, index: number) =>
            <Button
              key={`answer-${index}`}
              label={option}
              action={() => dispatch(submitAnswer([option]))}
            />
          )}
        </OptionsContainer>
      </PanelActions>
      <PanelDivider />
      <Actions />
    </Panel>
  )
}

export default Quiz
