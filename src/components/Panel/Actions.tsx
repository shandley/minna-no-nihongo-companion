import styled from '@emotion/styled'

export const PanelActions = styled.div`
  text-align: right;

  button + button {
    margin-left: 0.8rem;
  }
`