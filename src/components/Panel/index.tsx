import styled from '@emotion/styled'
import { theme } from '../../data'

interface IPanelProps {
  borderTop?: string
}

const Panel = styled.div<IPanelProps>`
  background-color: white;
  font-family: ${theme.fontFamily};
  padding: 0.9rem;
  margin-bottom: 18px;
  border-color: #e2e9e6;
  border-radius: 0;
  box-shadow: 0 1px 1px #d7d7d7;
  border-top: 3px solid ${props =>
    props.borderTop ? props.borderTop : 'transparent'
  };
`

export default Panel

export * from './Heading'
export * from './Divider'
export * from './Actions'
export * from './Icon'
