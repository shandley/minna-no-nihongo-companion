import * as React from 'react'
import { useState } from '../../hooks'
import Intro from '../Base/Quiz/Intro'
import Quiz from './Quiz'
import Results from '../Base/Quiz/Results'

const AdjectiveTester: React.FC<{}> = () => {
  const state = useState(s => s)

  const { activity } = state

  return activity.running
    ? <Quiz activity={activity} />
    : activity.complete
      ? <Results activity={activity} />
      : <Intro activity={activity} />
}

export default AdjectiveTester
