import * as React from 'react'
import styled from '@emotion/styled'
import { useDispatch } from 'react-redux'
import { IAdjectiveTesterQuestion } from '../../interfaces'
import { theme } from '../../data'
import { submitAnswer } from '../../store/actions/activity'
import Button from '../Button'
import Actions from '../Base/Quiz/Actions'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../Panel'
import { IActivityState } from '../../store/reducers/activity'

const OptionsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 0.7rem;

  button {
    letter-spacing: unset;
  }

  button + button {
    margin-left: 0;
  }
`

const Quiz: React.FC<{
  activity: IActivityState
}> = ({ activity }) => {
  const dispatch = useDispatch()

  const { correct, currentQuestion } = activity
  const { question, options, meta } = currentQuestion as IAdjectiveTesterQuestion

  const { green, red } = theme.colours

  const borderTop = typeof correct === "boolean"
    ? correct
      ? green.dark
      : red.dark
    : 'transparent'

  return (
    <Panel borderTop={borderTop}>
      <PanelHeading
        heading={`Q. ${question}`}
        subheading={meta.kanji}
      />
      <PanelDivider />
      <PanelActions>
        <OptionsContainer>
          {options.map((option: string, index: number) =>
            <Button
              key={`answer-${index}`}
              label={option}
              action={() => dispatch(submitAnswer([option]))}
            />
          )}
        </OptionsContainer>
      </PanelActions>
      <PanelDivider />
      <Actions />
    </Panel>
  )
}

export default Quiz
