import * as React from 'react'
import styled from '@emotion/styled'
import Text from './Text'

const Highlight = styled.span`
  color: red;
`

const HighlightedText: React.FC<{
  text: string,
  highlight: string
}> = ({ text, highlight }) => {
  const pieces = text.split(highlight)

  if (pieces.length > 1) {
    return (
      <Text>
        {
          pieces.map((p: string, index: number) => (
            <React.Fragment key={index}>
              {p}
              {index !== pieces.length -1 ? <Highlight>{highlight}</Highlight> : null}
            </React.Fragment>
          ))
        }
      </Text>
    )
  }

  return <Text>{text}</Text>
}

export default HighlightedText
