import * as React from 'react'
import styled from '@emotion/styled'
import { theme } from '../data'

const NavbarContainer = styled.nav`
  display: flex;
  width: 100%;
  height: 3.5rem;
  background-color: ${theme.colours.white};
  border-bottom: 1px solid ${theme.colours.grey.medium};
  font-family: ${theme.fontFamily};
  justify-content: center;

  p {
    display: flex;
    font-size: xx-large;
    margin: auto;
  }
`

const Navbar: React.FC<{}> = () => (
  <NavbarContainer><p>⛩</p></NavbarContainer>
)

export default Navbar