import styled from '@emotion/styled'
import { theme } from '../data'

const Text = styled.p`
  font-size: ${theme.fontSize.slightlySmaller};
  font-weight: lighter;
`

export default Text