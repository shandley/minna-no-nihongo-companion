import * as React from 'react'
import styled from '@emotion/styled'
import { connectModal, InjectedProps } from 'redux-modal'
import { theme } from '../data'

interface IModalContainerProps {
  show: boolean
}

const ModalWrapper = styled.div`
  display: flex;
  background-color: #ddd;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  transition: all .4s;
  justify-content: center;
  opacity: ${(props: IModalContainerProps) =>
    props.show ? '0.5' : '0'
  };
`

const ModalContent = styled.div`
  position: absolute;
  top: 3rem;
  margin: 1rem;
  transition: all .3s;
  width: calc(100% - 2rem);
  opacity: ${(props: IModalContainerProps) =>
    props.show ? '1' : '0'
  };
  max-width: ${theme.containers.main.maxWidth};
`

export const MODAL = 'modal'

const Modal: React.FC<InjectedProps & {
  content: any
}> = ({ show, content, handleDestroy }) => (
  <React.Fragment >
    <ModalWrapper onClick={handleDestroy} show={show} />
    <ModalContent show={show}>
      {content}
    </ModalContent>
  </React.Fragment>
)

export default connectModal({ name: MODAL, destroyOnHide: false })(Modal)