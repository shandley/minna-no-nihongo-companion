import styled from '@emotion/styled'
import { theme } from '../data'

const Select = styled.select`
  width: calc(100% - 10px);
  margin: 0;
  padding: 0.4rem 0.4rem 0.3rem;
  font-family: ${theme.fontFamily};
  font-size: ${theme.fontSize.slightlySmaller};
  font-weight: lighter;
  border: 1px solid #DDDDDD;
  -moz-appearance: none;
  -webkit-appearance: none;
`

export default Select