import * as React from 'react'
import styled from '@emotion/styled'
import { useDispatch } from 'react-redux'
import { show, destroy } from 'redux-modal'
import { setError } from '../../store/actions/error'
import { MODAL } from '../Modal'
import { ISentenceTesterQuestion, IGrammarRule } from '../../interfaces'
import { theme } from '../../data'
import { submitAnswer } from '../../store/actions/activity'
import Button from '../Button'
import Actions from '../Base/Quiz/Actions'
import History from '../Base/Quiz/History'
import Panel, { PanelHeading, PanelDivider, PanelActions, PanelIcon } from '../Panel'
import Input from '../Input'
import GrammarRuleItem from '../GrammarRuleItem'
import { withRuby } from '../../lib'
import { IActivityState } from '../../store/reducers/activity'
import Text from '../Text'

const StyledPanelActions = styled(PanelActions)`
  margin: 0.7rem 0 0 0;
`

const Instructions = styled(Text)`
  margin-top: 0;
`

const Quiz: React.FC<{
  activity: IActivityState
}> = ({ activity }) => {
  const inputEl: React.Ref<HTMLInputElement> = React.useRef(null)
  const dispatch = useDispatch()

  const checkEnterKey = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (event.key === 'Enter') {
      answer()
    }
  }

  const answer = () => {
    if (inputEl.current) {
      const el = inputEl.current as HTMLInputElement

      if (el.checkValidity()) {
        el.disabled = true

        dispatch(submitAnswer(el.value))

        setTimeout(() => {
          el.value = ''
          el.disabled = false
        }, 1000)
      }
    }
  }

  const showRule = (r: IGrammarRule) => {
    dispatch(show(MODAL, {
      content: <GrammarRuleItem
        rule={r}
        destroy={() => dispatch(destroy(MODAL))}
      />
    }))
  }

  const { correct, currentQuestion } = activity

  if (!currentQuestion) {
    dispatch(setError('No questions found'))

    return null
  }

  const { question, questionNumber, meta } = currentQuestion as ISentenceTesterQuestion
  const { instructions, rule } = meta

  const { green, red } = theme.colours

  const borderTop = typeof correct === "boolean"
    ? correct
      ? green.dark
      : red.dark
    : 'transparent'

  return (
    <Panel borderTop={borderTop}>
      <PanelIcon>
        <History question_id={currentQuestion.id} />
      </PanelIcon>
      {instructions ? <Instructions>{instructions}:</Instructions> : null}
      <PanelHeading
        heading={`Q.${questionNumber}: ${withRuby(question)}`}
        subheading={'(Ch. ' + rule.id + ')'}
      />
      <PanelDivider />
      <PanelActions>
        <Input ref={inputEl} required autoFocus type="text" id="answer" onKeyPress={checkEnterKey} />
      </PanelActions>
      <StyledPanelActions>
        <Button label="Show Rule" action={() => showRule(rule)} />
        <Button label='Submit' action={() => answer()} />
      </StyledPanelActions>
      <PanelDivider />
      <Actions />
    </Panel>
  )
}

export default Quiz
