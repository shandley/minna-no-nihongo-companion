import * as React from 'react'
import { ActivityType, IActivity } from '../interfaces'
import VerbConjugationTester from './VerbConjugationTester'
import GrammarTester from './GrammarTester'
import AdjectiveTester from './AdjectiveTester'
import SentenceTester from './SentenceTester'
import AdjectiveDictionary from './AdjectiveDictionary'
import VerbDictionary from './VerbDictionary'

/**
 * @TODO this better
 */
const Activity: React.FC<{ activity: IActivity }> = ({ activity }) => {
  switch (activity.type) {
    case ActivityType.TEST_GRAMMAR:
      return <GrammarTester />
    case ActivityType.TEST_VERB_CONJUGATION:
      return <VerbConjugationTester />
    case ActivityType.TEST_ADJECTIVES:
      return <AdjectiveTester />
    case ActivityType.SENTENCE_TESTER:
      return <SentenceTester />
    case ActivityType.DICT_ADJECTIVES:
      return <AdjectiveDictionary />
    case ActivityType.DICT_VERBS:
      return <VerbDictionary />
    default:
      return <p>Hmm, no activity matched. Got {activity.type} which is unknown.</p>
  }
}

export default Activity
