import * as React from 'react'
import styled from '@emotion/styled'
import Panel, { PanelHeading, PanelDivider, PanelActions } from './Panel'
import Button from './Button'
import { IGrammarRule } from '../interfaces'
import Text from './Text'

const Description = styled(Text)`
  line-height: 1.5em;
`

const GrammarRuleItem: React.FC<{
  rule: IGrammarRule,
  destroy: () => void
}> = ({ rule, destroy }) => {
  return (
    <Panel>
      <PanelHeading
        heading={`Chapter Reference: ${rule.id}`}
        subheading={rule.rule}
      />
      <PanelDivider />
      <Description><strong>Description</strong>: {rule.explanation}</Description>
      {rule.particles.length
        ? <p><strong>Particles</strong>: {rule.particles.join(', ')}</p>
        : null
      }
      <PanelDivider />
      <PanelActions>
        <Button
          label="Close"
          action={() => destroy()}
        />
      </PanelActions>
    </Panel>
  )
}

export default GrammarRuleItem