import {
  getAdjectiveQuestions
} from '../lib'

import {
  ActivityType,
  IAdjectiveTesterQuestion,
  INumQuestionOption,
  IQuizActivity,
} from '../interfaces'

import { numberOfQuestionsOption } from '../data/options'

import { equalArrays, numberedArrayItems } from '../utils'

export class AdjectiveTester implements IQuizActivity {
  public type = ActivityType.TEST_ADJECTIVES
  public title = 'Adjectives'
  public subtitle = 'Test adjective recognition'
  public options = [numberOfQuestionsOption]

  public evaluateAnswer = (answer: string[], question: IAdjectiveTesterQuestion) => ({
    correct: equalArrays(answer, question.answer)
  })

  public getQuestions = (
    options: INumQuestionOption
  ): Iterator<IAdjectiveTesterQuestion> => {
    const result = getAdjectiveQuestions(options)

    const items = numberedArrayItems(
      result.questions
    )

    return items[Symbol.iterator]()
  }

  public getFeedback = (question: IAdjectiveTesterQuestion, submittedAnswer: string[]) => null

  public renderQuestion = (question: IAdjectiveTesterQuestion) => {
    return question.question as string
  }
}
