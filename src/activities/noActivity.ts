import {
  ActivityType,
  IActivity,
  INumQuestionOption,
  IQuestion,
} from '../interfaces'

export class NoActivity implements IActivity {
  public type = ActivityType.NONE
  public title = 'Home'
  public subtitle = ''
  public options = []

  public evaluateAnswer = () => ({ correct: true })

  public getQuestions = (options?: INumQuestionOption) => {
    return [][Symbol.iterator]()
  }

  public getFeedback = (question: IQuestion, submittedAnswer: string[]) => null

  public renderQuestion = (question: IQuestion) => ''
}
