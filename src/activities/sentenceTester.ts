import {
  getSentenceQuestionItems,
  compareSentences,
  getSentenceQuestionFeedback
} from '../lib'

import {
  ActivityType,
  IQuizActivity,
  ISentenceTesterOptions,
  ISentenceTesterQuestion,
} from '../interfaces'

import {
  numberOfQuestionsOption,
  chapterRangeOptions,
} from '../data/options'

import { numberedArrayItems } from '../utils'

export class SentenceTester implements IQuizActivity {
  public type = ActivityType.SENTENCE_TESTER
  public title = 'Sentence Tester'
  public subtitle = 'Read English, type 日本語'
  public options = [...chapterRangeOptions, numberOfQuestionsOption]

  public evaluateAnswer = (answer: string, question: ISentenceTesterQuestion) => {
    return compareSentences(answer, question.answer.join(''))
  }

  public getQuestions = (
    options: ISentenceTesterOptions
  ): Iterator<ISentenceTesterQuestion> => {
    const result = getSentenceQuestionItems(options)

    const items = numberedArrayItems(
      result.questions
    )

    return items[Symbol.iterator]()
  }

  public getFeedback = (
    question: ISentenceTesterQuestion,
    submittedAnswer: string[]
  ) => getSentenceQuestionFeedback(
    question as ISentenceTesterQuestion,
    submittedAnswer.join('')
  )

  public renderQuestion = (question: ISentenceTesterQuestion) => {
    return question.question
  }
}
