export const enum ListAction {
  SET_ADJECTIVES = 'SET_ADJECTIVES',
  RESET_ADJECTIVES = 'RESET_ADJECTIVES',
  SET_VERBS = 'SET_VERBS',
  RESET_VERBS = 'RESET_VERBS'
}

export interface IListAction {
  type: ListAction
  payload?: any
}
