import {
  IGrammarItem,
  IGrammarRule,
} from '.'

import { ISentence } from '../lib'

export interface IQuestionCollection<T> {
  questions: T[],
  message?: string
}

export interface IQuestionResultFeedback {
  advice: string,
  match?: string
  missing?: string
  // submittedAnswer?: string[]
}

export interface IQuestionEvaluationResult {
  correct: boolean | null
  feedback?: IQuestionResultFeedback
}

export interface IQuestionResult {
  question: IQuestion
  submittedAnswer: string[]
  correct: boolean
  feedback?: IQuestionResultFeedback | null // @TODO might want to always return this, and not allow nulls
}

export interface IAdjectiveTesterQuestion extends IQuestion {
  question: string
  meta: {
    english: string
    kanji: string
  }
}

export interface IGrammarTesterQuestion extends IQuestion {
  question: IGrammarItem,
  meta: {
    ruleId: number
  }
}

export interface ISentenceTesterQuestion extends IQuestion {
  question: string,
  meta: {
    rule: IGrammarRule,
    sentence: ISentence
    instructions?: string 
  }
}

export interface IVerbConjugationTesterQuestion extends IQuestion {
  question: string
  meta: {
    english: string,
    kanji: string,
    instructions?: string
  }
}

export interface IQuestion {
  id: string
  question: any,
  options: string[],
  answer: string[],
  questionNumber?: number
}