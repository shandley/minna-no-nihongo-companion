export const enum OptionType {
  BOOLEAN,
  NUMBER,
  RANGE,
  STRING
}

export const enum OptionAction {
  SET_OPTION = 'SET_OPTION'
}

export interface IOptionItem {
  activityId: number
  optionKey: string
  optionValue: any
}

export type OptionPair = IOptionItem[]

export interface IOptionAction {
  type: OptionAction
  payload: IOptionItem
}

export type OptionDependencyRule = 'distinct' | 'min' | 'max'

/**
 * This belongs to an activity and has no connection to an IOptionItem
 */
export interface IActivityOption {
  readonly id: string,
  title: string,
  description?: string
  type: OptionType,
  default: any
  values?: Array<{
    label: string,
    value: string
  }>
  dependency?: {
    option: OptionKey,
    rule: OptionDependencyRule
  }
}

export const enum OptionKey {
  NUM_QUESTIONS = 'NUM_QUESTIONS',
  MIN_CHAPTER = 'MIN_CHAPTER',
  MAX_CHAPTER = 'MAX_CHAPTER',
  QUESTION_VERB_FORM = 'QUESTION_VERB_FORM',
  ANSWER_VERB_FORM = 'ANSWER_VERB_FORM'
}

export const enum VerbType {
  POLITE = 'polite_form',
  PLAIN = 'plain_form',
  NAI = 'nai_form',
  TE = 'te_form',
  TA = 'ta_form',
}

// @TODO This is data
export const VerbTypeLabels: { [key in VerbType]: string } = {
  'polite_form': 'ます Form',
  'plain_form': 'じしょう Form',
  'nai_form': 'ない Form',
  'te_form': 'て Form',
  'ta_form': 'た Form'
}

export interface INumQuestionOption {
  NUM_QUESTIONS: number
}

export interface IChapterRangeOptions {
  [OptionKey.MIN_CHAPTER]: number
  [OptionKey.MAX_CHAPTER]: number
}

export interface IVerbFormOptions {
  [OptionKey.QUESTION_VERB_FORM]: VerbType
  [OptionKey.ANSWER_VERB_FORM]: VerbType
}

export type IGrammarTesterOptions = IChapterRangeOptions & INumQuestionOption
export type ISentenceTesterOptions = IChapterRangeOptions & INumQuestionOption
export type IAdjectiveTesterOptions = INumQuestionOption
export type IVerbConjugationTesterOptions = INumQuestionOption & IVerbFormOptions
