import * as React from 'react'
import { render } from 'react-dom'
import App from './components/App'
import * as serviceWorker from './serviceWorker'
import { configureStore } from './store'
import { Provider } from 'react-redux'

render(
  <Provider store={configureStore()}>
    <App />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
