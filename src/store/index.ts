import {
  combineReducers,
  createStore,
  Store,
  applyMiddleware
} from 'redux'

import thunk from 'redux-thunk'
import logger from 'redux-logger' // @TODO disable in production

import { reducer as activity, IActivityState } from './reducers/activity'
import { reducer as list, IListState } from './reducers/list'
import { reducer as modal, ReduxModalState } from 'redux-modal'
import { reducer as error, IErrorState } from './reducers/error'

import { particleTesterMiddleware } from './middleware/particleTester'
import { persistOptionsMiddleware } from './middleware/persistOptions'
import { persistResultsMiddleware } from './middleware/persistResults'

export interface IApplicationState {
  activity: IActivityState,
  list: IListState,
  modal: ReduxModalState,
  error: IErrorState
}

const configureStore = (): Store<Record<string, any>> => createStore(
  combineReducers({ activity, list, modal, error }),
  {},
  applyMiddleware(...[
    thunk,
    logger,
    particleTesterMiddleware,
    persistOptionsMiddleware,
    persistResultsMiddleware
  ])
)

export { configureStore }
