import { ActionCreator } from 'redux'
import { IApplicationState } from ".."
import { NoActivity } from '../../activities/noActivity'
import { IActivityState } from '../reducers/activity'
import {
  ActivityAction,
  IActivity,
  IActivityAction,
  IOptionItem,
} from '../../interfaces'

type IActionCreator = ActionCreator<IActivityAction>
type Thunk<T> = (args?: any) => (dispatch: (action: T) => void, getState: () => IApplicationState) => void
type ActivityThunk = Thunk<IActivityAction>

const NEXT_QUESTION_TIMEOUT_MS = 1000

export const setActivity: IActionCreator = (activity: IActivity) => ({
  type: ActivityAction.SET_ACTIVITY,
  payload: activity
})

export const backToMenu: IActionCreator = () => ({
  type: ActivityAction.SET_ACTIVITY,
  payload: new NoActivity()
})

export const startActivity: IActionCreator = () => ({
  type: ActivityAction.START
})

export const submitAnswer: ActivityThunk = (payload) => {
  return (dispatch, getState) => {
    const { activity } = getState()

    dispatch({
      type: ActivityAction.SUBMIT_ANSWER,
      payload
    })

    dispatch(recordResult)
    transtitionToNextQuestion(activity, dispatch)
  }
}

const transtitionToNextQuestion = (
  activity: IActivityState,
  dispatch: (args: any) => void,
  timeout: number = NEXT_QUESTION_TIMEOUT_MS
) => {
  const nextQuestion = activity.questions.next()

  setTimeout(() => {
    if (!nextQuestion.done) {
      dispatch({
        type: ActivityAction.NEXT_QUESTION,
        payload: nextQuestion.value
      })
    } else {
      dispatch({
        type: ActivityAction.COMPLETE,
      })
    }
  }, timeout)
}

export const unmaskQuestion = () => ({
  type: ActivityAction.UNMASK_QUESTION,
})

export const recordResult = ({
  type: ActivityAction.RECORD_RESULT
})

export const restartActivity: ActivityThunk = () => {
  return (dispatch) => {
    dispatch({
      type: ActivityAction.RESET
    })

    dispatch({
      type: ActivityAction.START
    })
  }
}

export const quitActivity: ActivityThunk = () => {
  return (dispatch) => {
    dispatch({
      type: ActivityAction.RESET
    })

    dispatch(setActivity(new NoActivity()))
  }
}

export const skipQuestion: ActivityThunk = () => {
  return (dispatch, getState) => {
    const { activity } = getState()

    dispatch({
      type: ActivityAction.SKIP_QUESTION,
    })

    transtitionToNextQuestion(activity, dispatch)
  }
}

export const setOption: IActionCreator = (payload: IOptionItem) => ({
  type: ActivityAction.SET_OPTION,
  payload
})
