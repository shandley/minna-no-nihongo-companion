import { ActionCreator } from 'redux'
import { ListAction, IListAction } from '../../interfaces'
import { Adjective } from 'minna-no-nihongo-adjectives'
import { Verb } from 'minna-no-nihongo-verbs'

type IActionCreator = ActionCreator<IListAction>

export const setAdjectives: IActionCreator = (payload: Adjective[]) => ({
  type: ListAction.SET_ADJECTIVES,
  payload
})

export const resetAdjectives: IActionCreator = () => ({
  type: ListAction.RESET_ADJECTIVES
})

export const setVerbs: IActionCreator = (payload: Verb[]) => ({
  type: ListAction.SET_VERBS,
  payload
})

export const resetVerbs: IActionCreator = () => ({
  type: ListAction.RESET_VERBS
})
