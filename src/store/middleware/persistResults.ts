import { Store } from 'redux'
import { ActivityAction, IActivityAction } from '../../interfaces'
import { persistResult } from '../../lib/storage'
import { IApplicationState } from '..'

/**
 * This middleware captures any answer results and persists them for stats purposes 
 */
export const persistResultsMiddleware = (store: Store) => (next: any) => (action: IActivityAction) => {
  const { type } = action

  if (type !== ActivityAction.RECORD_RESULT) {
    return next(action)
  }

  const { activity } = store.getState() as IApplicationState

  persistResult({
    question: activity.currentQuestion,
    submittedAnswer: activity.submittedAnswer,
    correct: activity.correct as boolean,
  })

  return next(action)
}
