import { ErrorAction, IErrorAction } from '../../interfaces'

export interface IErrorState {
  error: string | null
}

export const initialState: IErrorState = {
  error: null
}

export const reducer = (
  state: IErrorState = initialState,
  action: IErrorAction
): IErrorState => {
  switch (action.type) {
    case ErrorAction.SET_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state
  }
}
