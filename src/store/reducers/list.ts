import adjectives, { Adjective } from 'minna-no-nihongo-adjectives'
import verbs, { Verb } from 'minna-no-nihongo-verbs'
import { ListAction } from '../../interfaces'

export interface IListState {
  adjectives: Adjective[],
  verbs: Verb[],
  filteredAdjectives: Adjective[],
  filteredVerbs: Verb[]
}

export const initialState: IListState = {
  adjectives,
  verbs,
  filteredAdjectives: adjectives,
  filteredVerbs: verbs
}

export const reducer = (
  state: IListState = initialState,
  action: {
    type: ListAction
    payload?: Array<Adjective | Verb>
  }
): IListState => {
  switch (action.type) {
    case ListAction.SET_ADJECTIVES:
      return {
        ...state,
        filteredAdjectives: action.payload as Adjective[]
      }
    case ListAction.RESET_ADJECTIVES:
      return {
        ...state,
        filteredAdjectives: initialState.adjectives
      }
    case ListAction.SET_VERBS:
      return {
        ...state,
        filteredVerbs: action.payload as Verb[]
      }
    case ListAction.RESET_VERBS:
      return {
        ...state,
        filteredVerbs: initialState.verbs
      }
    default:
      return state
  }
}
